package and.digital.iLab.Design.patterns.facade.seba;

import java.time.LocalDate;

public class Bicycle {

    private BreakingType breakingType;
    private BicycleFrame bicycleFrame;
    private Owner owner;

    public Bicycle(BreakingType breakingType, BicycleFrame bicycleFrame, Owner owner) {
        this.breakingType = breakingType;
        this.bicycleFrame = bicycleFrame;
        this.owner = owner;
    }

    public Bicycle(String breakingType, int wheelSize, int numOfLights, String bicycleType,
                   String ownerName, LocalDate rentedSince) {
        this.breakingType = BreakingType.fromName(breakingType);
        this.bicycleFrame = new BicycleFrame(wheelSize, numOfLights, BicycleType.fromName(bicycleType));
        this.owner = new Owner(ownerName, rentedSince);
    }

}
