package and.digital.iLab.Design.patterns.builder.andrey.java;

public interface TransportBuilder {
  public TransportBuilder reset();

  public Transport getResult();

  public void addBrand(String brand);

  public void addChassis(Material material);

  public void addEngine(Engine engine);

  public void addRoof(Material material);

  public void addControls(Controls controlsType);
}
