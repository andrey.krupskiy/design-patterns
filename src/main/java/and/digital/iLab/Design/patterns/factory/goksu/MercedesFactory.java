package and.digital.iLab.Design.patterns.factory.goksu;

/**
 * @author goksu
 * @created 17/06/2022
 * MercedesFactory
 **/

public class MercedesFactory extends CarFactory {
  @Override
  public Car createCar(String model) {
    if (model.equals("e")) {
      return new MercedesE(model);
    } else {
      return new MercedesS(model);
    }
  }
}
