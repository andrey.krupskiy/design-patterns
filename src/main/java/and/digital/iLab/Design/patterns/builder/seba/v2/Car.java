package and.digital.iLab.Design.patterns.builder.seba.v2;

public class Car {

    private String brand;
    private int numOfWheels;
    private int doors;
    private int wheelSize;
    private boolean brandNew;
    private int numOfSeats;
    private boolean convertible;
    private int maxSpeed;

    private Car(String brand, int numOfWheels, int doors, int wheelSize, boolean brandNew, int numOfSeats, boolean convertible, int maxSpeed) {
        this.brand = brand;
        this.numOfWheels = numOfWheels;
        this.doors = doors;
        this.wheelSize = wheelSize;
        this.brandNew = brandNew;
        this.numOfSeats = numOfSeats;
        this.convertible = convertible;
        this.maxSpeed = maxSpeed;
    }

    public String getBrand() {
        return brand;
    }

    public int getNumOfWheels() {
        return numOfWheels;
    }

    public int getDoors() {
        return doors;
    }

    public int getWheelSize() {
        return wheelSize;
    }

    public boolean isBrandNew() {
        return brandNew;
    }

    public int getNumOfSeats() {
        return numOfSeats;
    }

    public boolean isConvertible() {
        return convertible;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public static Builder newCar(){
        return new Builder(true);
    }

    public static Builder usedCar(){
        return new Builder(false);
    }

    public static class Builder{

        private String brand;
        private int numOfWheels;
        private int doors;
        private int wheelSize;
        private boolean brandNew;
        private int numOfSeats;
        private boolean convertible;
        private int maxSpeed;

        private Builder(boolean brandNew){
            this.brandNew = brandNew;
        }

        public Builder withBrand(String brand) {
            this.brand = brand;
            return this;
        }

        public Builder withNumOfWheels(int numOfWheels) {
            this.numOfWheels = numOfWheels;
            return this;
        }

        public Builder withDoors(int doors) {
            this.doors = doors;
            return this;
        }

        public Builder withWheelSize(int wheelSize) {
            this.wheelSize = wheelSize;
            return this;
        }

        public Builder withNumOfSeats(int numOfSeats) {
            this.numOfSeats = numOfSeats;
            return this;
        }

        public Builder withConvertible(boolean convertible) {
            this.convertible = convertible;
            return this;
        }

        public Builder withMaxSpeed(int maxSpeed) {
            this.maxSpeed = maxSpeed;
            return this;
        }

        public Car build(){
            return new Car(brand, numOfWheels, doors, wheelSize, brandNew, numOfSeats, convertible, maxSpeed);
        }
    }

}
