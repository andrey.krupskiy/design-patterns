package and.digital.iLab.Design.patterns.prototype.boyd;

public class House implements Cloneable {
    private int size;
    Room[] rooms;

    public House(int size, Room[] rooms) {
        this.size = size;
        this.rooms = rooms;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setRooms(Room[] rooms) {
        this.rooms = rooms;
    }

    public Room[] getRooms() {
        return rooms;
    }

    @Override
    public House clone() {
        try {
            House clone = (House) super.clone();
            Room[] newRooms = new Room[rooms.length];
            for (int i = 0; i < rooms.length; i++) {
                newRooms[i] = new Room(rooms[i].getSize(), rooms[i].getType());
            }
            clone.rooms = newRooms;
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
