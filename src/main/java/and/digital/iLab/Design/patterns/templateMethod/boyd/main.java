package and.digital.iLab.Design.patterns.templateMethod.boyd;

public class main {
    public static void main(String[] args) {
        EShop eShop = new EShop();
        PhysicalShop physicalShop = new PhysicalShop();

        eShop.customerCycle();
        physicalShop.customerCycle();
    }
}
