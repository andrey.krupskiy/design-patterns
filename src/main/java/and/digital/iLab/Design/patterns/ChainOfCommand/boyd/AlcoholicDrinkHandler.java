package and.digital.iLab.Design.patterns.ChainOfCommand.boyd;

public class AlcoholicDrinkHandler extends DrinkHandler {
    public void drink(String type) {
        if (type.equals("Alcoholic")) {
            System.out.println("Drinking a alcoholic drink");
            return;
        }
        super.drink(type);
    }

}
