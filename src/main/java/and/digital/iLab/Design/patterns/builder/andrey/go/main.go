package main

import "fmt"

func main() {

	b := CarBuilder{}
	d := Director{}

	d.CreateSportsCar(&b)
	sportsCar := b.GetResult()

	fmt.Println(sportsCar)
	fmt.Println(sportsCar.MakeSound())
	fmt.Println(sportsCar.Transport())

}
