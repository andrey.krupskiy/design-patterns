package and.digital.iLab.Design.patterns.proxy.goksu;

/**
 * @author goksu
 * @created 27/06/2022
 * main
 **/

public class main {
  public static void main(String[] args) {
    Car car = new CarProxy();
    car.assemble();
  }
}
