package and.digital.iLab.Design.patterns.factory.boyd;

public abstract class ComputerFactory {
    public abstract Computer createComputer(String supplier);
}
