package and.digital.iLab.Design.patterns.iterator.boyd;

import java.util.Iterator;

public class People implements Iterable{

    private Person[] people;

    public People(Person...people) {
        this.people = people;
    }

    @Override
    public PeopleIterator iterator() {
        return new PeopleIterator(this);
    }

    public Person[] getPeople() {
        return people;
    }
}
