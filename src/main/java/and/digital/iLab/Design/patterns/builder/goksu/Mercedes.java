package and.digital.iLab.Design.patterns.builder.goksu;

/**
 * @author goksu
 * @created 21/06/2022
 * Mercedes
 **/

public class Mercedes extends Car {
  private final String model;

  private Mercedes(Builder builder) {
    super(builder);
    model = builder.model;
  }

  @Override
  public String toString() {
    return "Mercedes{" +
        "accessories=" + accessories +
        ", model='" + model + '\'' +
        '}';
  }

  protected static class Builder extends Car.Builder<Builder> {
    private String model;
    public Builder(String model) {
      this.model = model;
    }

    @Override
    Builder self() {
      return this;
    }

    @Override
    public Mercedes build() {
      return new Mercedes(this);
    }
  }
}
