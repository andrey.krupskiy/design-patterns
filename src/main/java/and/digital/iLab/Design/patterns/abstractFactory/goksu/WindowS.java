package and.digital.iLab.Design.patterns.abstractFactory.goksu;

/**
 * @author goksu
 * @created 20/06/2022
 * WindowS
 **/

public class WindowS implements Window {
  @Override
  public String toString() {
    return "WindowS";
  }
}
