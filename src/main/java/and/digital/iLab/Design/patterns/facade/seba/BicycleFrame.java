package and.digital.iLab.Design.patterns.facade.seba;

public class BicycleFrame {

    private int wheelSize;
    private int numOfLights;
    private BicycleType bicycleType;

    public BicycleFrame(int wheelSize, int numOfLights, BicycleType bicycleType) {
        this.wheelSize = wheelSize;
        this.numOfLights = numOfLights;
        this.bicycleType = bicycleType;
    }

    public int getWheelSize() {
        return wheelSize;
    }

    public int getNumOfLights() {
        return numOfLights;
    }

    public BicycleType getBicycleType() {
        return bicycleType;
    }
}
