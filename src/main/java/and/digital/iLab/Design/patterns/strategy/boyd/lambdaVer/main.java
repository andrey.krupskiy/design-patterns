package and.digital.iLab.Design.patterns.strategy.boyd.lambdaVer;

public class main {
    public static void main(String[] args) {
        StrategyUser strategyUser = new StrategyUser();
        strategyUser.useStrategy(Strategy.strategyA);
        strategyUser.useStrategy(Strategy.strategyB);
    }
}
