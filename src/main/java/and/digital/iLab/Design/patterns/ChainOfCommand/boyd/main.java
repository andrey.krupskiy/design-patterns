package and.digital.iLab.Design.patterns.ChainOfCommand.boyd;

public class main {
    public static void main(String[] args) {
        DrinkHandler drinkHandler = DrinkHandler.link(
                new AlcoholicDrinkHandler(),
                new NonAlcoholicDrinkHandler(),
                new WaterHandler()
        );

        drinkHandler.drink("Water");
        drinkHandler.drink("Alcoholic");
        drinkHandler.drink("Non-alcoholic");
    }
}
