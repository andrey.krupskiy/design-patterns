package and.digital.iLab.Design.patterns.iterator.boyd;

public class Person {
    String name;
    int age;
    boolean isAlive;

    public Person(String name, int age, boolean isAlive) {
        this.name = name;
        this.age = age;
        this.isAlive = isAlive;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public boolean isAlive() {
        return isAlive;
    }
}
