package main

type Transport interface {
	MakeSound() string
}
