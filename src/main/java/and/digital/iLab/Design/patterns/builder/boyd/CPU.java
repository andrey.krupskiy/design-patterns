package and.digital.iLab.Design.patterns.builder.boyd;

public class CPU {
    private String brand;
    private Sockets socket;

    private boolean hasIntegratedGraphics;


    public CPU(String brand, Sockets socket, boolean hasIntegratedGraphics) {
        this.brand = brand;
        this.socket = socket;
        this.hasIntegratedGraphics = hasIntegratedGraphics;
    }

    public Sockets getSocket() {
        return socket;
    }

    public String getBrand() {
        return brand;
    }

    public boolean hasIntegratedGraphics() {
        return hasIntegratedGraphics;
    }

    @Override
    public String toString() {
        return "CPU: " + brand + ", " + socket + ", " + hasIntegratedGraphics;
    }
}

