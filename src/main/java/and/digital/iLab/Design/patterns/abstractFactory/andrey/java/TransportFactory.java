package and.digital.iLab.Design.patterns.abstractFactory.andrey.java;

public interface TransportFactory {
  public Transport CreateTransport(String brand);
}
