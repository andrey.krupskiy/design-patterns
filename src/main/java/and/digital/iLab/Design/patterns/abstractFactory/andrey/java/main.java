package and.digital.iLab.Design.patterns.abstractFactory.andrey.java;

public class main {

  public static void main(String[] args) {
    ModernCarFactory f = new ModernCarFactory();
    Transport car = f.CreateTransport("BMW");

    System.out.println(car.MakeSound());
  }
}
