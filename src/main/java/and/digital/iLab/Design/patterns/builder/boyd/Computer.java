package and.digital.iLab.Design.patterns.builder.boyd;

public class Computer {
    private final CPU cpu;
    private final Motherboard motherboard;
    private final String gpuBrand;
    private final int amountOfRamSticks;
    private final int amountOfFans;
    private final boolean isDusty;

    private Computer(CPU cpu, Motherboard motherBoard, String gpuBrand, int amountOfRamSticks, int amountOfFans, boolean isDusty){
        this.cpu = cpu;
        this.motherboard = motherBoard;
        this. gpuBrand = gpuBrand;
        this.amountOfFans = amountOfFans;
        this.amountOfRamSticks = amountOfRamSticks;
        this.isDusty = isDusty;
    }

    public CPU getCpu() {
        return cpu;
    }

    public Motherboard getMotherboard() {
        return motherboard;
    }

    private String getGpuBrand() {
        return gpuBrand;
    }

    public int getAmountOfRamSticks() {
        return amountOfRamSticks;
    }

    public int getAmountOfFans() {
        return amountOfFans;
    }

    @Override
    public String toString() {
        return "Computer: " + this.cpu.toString() + ", " + this.motherboard.toString()  + ", " + this.gpuBrand + ", " + this.amountOfRamSticks +
                ", " + this.amountOfFans + ", " + this.isDusty;
    }

    public boolean isDusty() {
        return isDusty;
    }

    public static class ComputerBuilder {

        private final CPU cpu;
        private final Motherboard motherboard;
        private  final int amountOfRamSticks;


        private String gpuBrand;
        private int amountOfFans;
        private boolean isDusty = false;

        public ComputerBuilder(CPU cpu, Motherboard motherboard, int amountOfRamSticks) {
            this.cpu = cpu;
            this.motherboard = motherboard;
            this.amountOfRamSticks = amountOfRamSticks;
        }

        public ComputerBuilder withGpuBrand(String gpuBrand) {
            this.gpuBrand = gpuBrand;
            return this;
        }

        public ComputerBuilder withAmountOfFans(int amountOfFans) {
            this.amountOfFans = amountOfFans;
            return this;
        }

        public ComputerBuilder withIsDusty(boolean isDusty) {
            this.isDusty = isDusty;
            return this;
        }

        private String validate() {
            if(cpu.getSocket() != motherboard.getSocket()) {
                return "CPU is not compatible with motherboard";
            };

            if (!cpu.hasIntegratedGraphics() && gpuBrand == null) {
                return "CPU has no integrated graphics must have a graphics card";
            }
            return null;
        }

        public Computer build(){
            String validation = validate();
            if (validation != null) {
                System.out.println(validation);
                return null;
            }
            return new Computer(cpu, motherboard, gpuBrand, amountOfRamSticks, amountOfFans, isDusty);
        }
    }
}
