package and.digital.iLab.Design.patterns.facade.goksu;

/**
 * @author goksu
 * @created 24/06/2022
 * ProcessFacade
 **/

public class ProcessFacade {
  private ProcessController processController = new ProcessController();


  public void runStartProcessesInAlphabeticalOrder(){
    processController.startA();
    processController.startB();
    processController.startC();
    processController.startD();
    processController.startE();
  }

  public void runStopProcessesInAlphabeticalOrder(){
    processController.stopA();
    processController.stopB();
    processController.stopC();
    processController.stopD();
    processController.stopE();
  }
}
