package and.digital.iLab.Design.patterns.singleton.seba;

public class Singleton {

    private static Singleton INSTANCE;

    private String value;

    private Singleton(String value) {
        this.value = value;
    }

    public synchronized static Singleton getInstance(String value) {
        if (INSTANCE == null) {
            INSTANCE = new Singleton(value);
        }
        return INSTANCE;
    }

    public String getValue() {
        return value;
    }
}
