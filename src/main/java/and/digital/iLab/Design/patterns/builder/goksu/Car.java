package and.digital.iLab.Design.patterns.builder.goksu;

import java.util.ArrayList;
import java.util.List;

/**
 * @author goksu
 * @created 21/06/2022
 * Car
 **/

public abstract class Car {
  final List<String> accessories;

  Car(Builder<?> builder) {
    accessories = new ArrayList<>(builder.accessories);
  }

  protected abstract static class Builder<T extends Builder<T>>{
    List<String> accessories = new ArrayList<>();
    public T addAccessory(String accessory){
      accessories.add(accessory);
      return self();
    }
    abstract T self();
    abstract Car build();
  }


}
