package and.digital.iLab.Design.patterns.abstractFactory.goksu;

/**
 * @author goksu
 * @created 20/06/2022
 * WheelE
 **/

public class WheelE implements Wheel {
  @Override
  public String toString() {
    return "WheelE";
  }
}
