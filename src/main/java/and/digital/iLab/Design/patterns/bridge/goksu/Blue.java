package and.digital.iLab.Design.patterns.bridge.goksu;

/**
 * @author goksu
 * @created 22/06/2022
 * Blue
 **/

public class Blue implements Colour {
  @Override
  public String getColour() {
    return "Blue";
  }
}
