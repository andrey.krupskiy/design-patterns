package and.digital.iLab.Design.patterns.builder.boyd;

public class main {
    public static void main(String[] args) {
        CPU amdCPU = new CPU("AMD", Sockets.AM4, true);
        Motherboard Asus = new Motherboard("Asus", Sockets.AM4);
        Computer computer = new Computer.ComputerBuilder(amdCPU, Asus, 3)
                .withAmountOfFans(4)
                .withIsDusty(true)
                .build();

        System.out.println(computer.toString());
    }
}
