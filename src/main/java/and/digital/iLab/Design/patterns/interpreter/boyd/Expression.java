package and.digital.iLab.Design.patterns.interpreter.boyd;

public interface Expression {
    boolean interpreter(String context);
}
