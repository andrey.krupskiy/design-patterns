package and.digital.iLab.Design.patterns.templateMethod.boyd;

public class EShop extends Shop {

    @Override
    public void sellItem() {
        System.out.println("Customer pays with paypal");
        super.sellItem();
    }

    public void deliver() {
        System.out.println("Packaged item");
        System.out.println("Send item out for delivery with postal service");
    }
}
