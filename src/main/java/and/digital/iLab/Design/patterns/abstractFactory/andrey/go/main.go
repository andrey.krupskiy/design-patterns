package main

import "fmt"

func main() {
	fmt.Println("Starting abstraction factory program...")

	f, err := CreateFactory("car")
	if err != nil {
		fmt.Println(err)
	}

	c := f.CreateTransport("Mitsubishi")
	fmt.Println(c.MakeSound())

	f, err = CreateFactory("boat")
	if err != nil {
		fmt.Println(err)
	}

}
