package and.digital.iLab.Design.patterns.prototype.seba.v2;

import java.util.StringJoiner;

public class Address2 {

    private String street;
    private int num;

    public Address2(String street, int num) {
        this.street = street;
        this.num = num;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Address2.class.getSimpleName() + "[", "]")
                .add("street2='" + street + "'")
                .add("num2=" + num)
                .toString();
    }
}
