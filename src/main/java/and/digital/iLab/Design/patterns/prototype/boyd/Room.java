package and.digital.iLab.Design.patterns.prototype.boyd;

public class Room {
    private int size;
    private String type;

    public Room(int size, String type) {
        this.size = size;
        this.type = type;
    }

    public int getSize() {
        return size;
    }

    public String getType() {
        return type;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setType(String type) {
        this.type = type;
    }
}
