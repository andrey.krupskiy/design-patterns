package and.digital.iLab.Design.patterns.factory.boyd;

public class DesktopFactory extends ComputerFactory {
    @Override
    public Computer createComputer(String supplier) {
        switch (supplier) {
            case "NZXT":
                return new NZXTDesktop(supplier);
            case "Alienware":
                return new AlienwareDesktop(supplier);
            default:
                return null;
        }
    }
}
