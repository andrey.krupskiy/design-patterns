package main

type RoundHole struct {
	radius float32
}

func NewRoundHole(radius float32) RoundHole {
	return RoundHole{radius: radius}
}

func (rh RoundHole) Radius() float32 {
	return rh.radius
}

func (rh RoundHole) Fits(rp RoundPeg) bool {
	if rh.radius < rp.Radius() {
		return false
	}
	return true
}
