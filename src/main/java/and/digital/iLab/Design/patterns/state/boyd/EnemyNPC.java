package and.digital.iLab.Design.patterns.state.boyd;

import and.digital.iLab.Design.patterns.state.boyd.states.IdleState;
import and.digital.iLab.Design.patterns.state.boyd.states.State;

public class EnemyNPC {

    private State state;
    private boolean isAggressed;
    private String currentAction;

    public EnemyNPC() {
        this.state = new IdleState(this);
        this.isAggressed = false;
        this.currentAction = "Idling...";
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public void setAggressed(boolean aggressed) {
        isAggressed = aggressed;
    }

    public void setCurrentAction(String currentAction) {
        this.currentAction = currentAction;
    }

    public String getCurrentAction() {
        return currentAction;
    }

    public boolean isAggressed() {
        return isAggressed;
    }
}
