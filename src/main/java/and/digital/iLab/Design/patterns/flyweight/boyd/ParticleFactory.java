package and.digital.iLab.Design.patterns.flyweight.boyd;

import java.util.HashMap;
import java.util.Map;

public class ParticleFactory {
    static Map<String, ParticleType> particleTypes = new HashMap<>();

    public static ParticleType getParticleType(String name, int size) {
        ParticleType result = particleTypes.get(name);
        if (result == null) {
            result = new ParticleType(name, size);
            particleTypes.put(name, result);
        }
        return result;
    }
}
