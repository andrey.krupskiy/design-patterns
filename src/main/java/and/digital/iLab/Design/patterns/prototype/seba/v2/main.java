package and.digital.iLab.Design.patterns.prototype.seba.v2;

public class main {

  public static void main(String[] args) {
      Address2 address = new Address2("streetName1", 1);

      Person2 person = new Person2("person1", address);

      Person2 clone = Person2.clone(person);
      clone.setName("changed");
      clone.getAddress().setStreet("changed streetName");

      address.setNum(2);

    System.out.println(person.toString());
    System.out.println(clone.toString());
  }

}
