package main

import "fmt"

func main() {
	c1 := GetDBClient("FOO")
	c2 := GetDBClient("BAR")

	fmt.Println("Client 1: ", c1.CreatedAt, c1.DBString)
	fmt.Println("Client 2: ", c2.CreatedAt, c2.DBString)

}
