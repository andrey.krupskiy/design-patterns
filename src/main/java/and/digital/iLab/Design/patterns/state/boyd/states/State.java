package and.digital.iLab.Design.patterns.state.boyd.states;

import and.digital.iLab.Design.patterns.state.boyd.EnemyNPC;

public abstract class State {
    EnemyNPC enemyNPC;

    State(EnemyNPC enemyNPC) {
        this.enemyNPC = enemyNPC;
    }

    public abstract void onIdle();
    public abstract void onSearching();
    public abstract void onChasing();
}
