package main

import "fmt"

type Material uint

const (
	metal Material = iota
	wood
	plastic
	glass
)

func (m Material) String() string {
	switch m {
	case metal:
		return "metal"
	case wood:
		return "wood"
	case plastic:
		return "plastic"
	case glass:
		return "glass"
	default:
		return "unknown"
	}
}

type Controls uint

const (
	analogue Controls = iota
	digital
)

func (c Controls) String() string {
	switch c {
	case analogue:
		return "analogue"
	case digital:
		return "digital"
	default:
		return "unknown"
	}
}

type Car struct {
	brand    string
	chassis  Material
	engine   IEngine
	roof     Material
	Controls Controls
}

func NewCar(brand string, chassis Material, engine IEngine, roof Material, controls Controls) Car {
	return Car{brand, chassis, engine, roof, controls}
}

func (c Car) MakeSound() string {
	return fmt.Sprintf("Car goes %s", c.engine.ExertPower())
}

func (c Car) Transport() string {
	return fmt.Sprint("You have arrived at your destination")
}

func (c Car) String() string {
	return fmt.Sprintf("A %s car with %s chassis, %s engine, %s roof and %s controls", c.brand, c.chassis, c.engine, c.roof, c.Controls)
}
