package and.digital.iLab.Design.patterns.abstractFactory.goksu;

/**
 * @author goksu
 * @created 20/06/2022
 * CarPartsFactory
 **/

public interface CarPartsFactory {

  Window createWindow();

  Wheel createWheel();

  SteeringWheel createSteeringWheel();

}
