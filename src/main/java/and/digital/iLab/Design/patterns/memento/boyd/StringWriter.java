package and.digital.iLab.Design.patterns.memento.boyd;

public class StringWriter {
    private String string;
    private final Memento memento;

    public StringWriter(String string) {
        this.string = string;
        this.memento = new Memento(string);
    }

    public void setString(String string) {
        memento.setState(this.string);
        this.string = string;
    }

    public void undo() {
        string = memento.getState();
    }

    public String getString() {
        return string;
    }
}
