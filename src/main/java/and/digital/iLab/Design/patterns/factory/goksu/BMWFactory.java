package and.digital.iLab.Design.patterns.factory.goksu;


/**
 * @author goksu
 * @created 17/06/2022
 * BMWFactory
 **/

public class BMWFactory extends CarFactory {

  @Override
  public Car createCar(String model) {
    return new BMW1(model);
  }
}
