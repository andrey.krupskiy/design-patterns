package and.digital.iLab.Design.patterns.builder.andrey.java;

public class main {
  public static void main(String[] args) {
    TransportBuilder b = new CarBuilder();
    Director d = new Director();
    d.constructSportsCar(b);
    Transport car = b.getResult();

    System.out.println(car);
    System.out.println(car.makeSound());
    car.transport();
  }
}
