package and.digital.iLab.Design.patterns.builder.seba.v2;

public class main {

  public static void main(String[] args) {

      Car.Builder tesla = Car.newCar()
              .withBrand("Tesla")
              .withConvertible(true)
              .withDoors(2)
              .withMaxSpeed(Integer.MAX_VALUE)
              .withNumOfWheels(4)
              .withNumOfSeats(5)
              .withWheelSize(21);

      Car build = tesla.build();
  }
}
