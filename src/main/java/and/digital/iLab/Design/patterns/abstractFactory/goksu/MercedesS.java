package and.digital.iLab.Design.patterns.abstractFactory.goksu;

/**
 * @author goksu
 * @created 20/06/2022
 * MercedesS
 **/

public class MercedesS extends Car {

  private CarPartsFactory carPartsFactory;

  public MercedesS(String model, CarPartsFactory carPartsFactory) {
    super(model);
    this.carPartsFactory = carPartsFactory;
  }

  @Override
  void assemble() {
    System.out.println("Assembling MercedesS");
    steeringWheel = carPartsFactory.createSteeringWheel();
    wheel = carPartsFactory.createWheel();
    window = carPartsFactory.createWindow();
  }
}
