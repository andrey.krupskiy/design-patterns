package and.digital.iLab.Design.patterns.bridge.goksu;

/**
 * @author goksu
 * @created 22/06/2022
 * Colour
 **/

public interface Colour {
  String getColour();
}
