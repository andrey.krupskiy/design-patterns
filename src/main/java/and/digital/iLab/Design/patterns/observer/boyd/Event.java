package and.digital.iLab.Design.patterns.observer.boyd;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class Event {
    private String state = "";
    private final PropertyChangeSupport support = new PropertyChangeSupport(this);

    public void setNewState(String state) {
        support.firePropertyChange("state", this.state, state);
        this.state = state;
    }

    public void addListener(PropertyChangeListener listener) {
        support.addPropertyChangeListener(listener);
    }
}
