package and.digital.iLab.Design.patterns.ChainOfCommand.boyd;

public class NonAlcoholicDrinkHandler extends DrinkHandler{
    public void drink(String type) {
        if (type.equals("Non-alcoholic")) {
            System.out.println("Drinking a non alcoholic drink");
            return;
        }
        super.drink(type);
    }
}
