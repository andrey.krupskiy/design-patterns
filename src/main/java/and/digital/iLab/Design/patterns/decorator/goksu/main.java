package and.digital.iLab.Design.patterns.decorator.goksu;

/**
 * @author goksu
 * @created 23/06/2022
 * main
 **/

public class main {
  public static void main(String[] args) {
    Car mercedes = new Mercedes();
    System.out.println("Cost of mercedes : " + mercedes.computeCost());
    mercedes = new LeatherSeatDecorator(mercedes);
    System.out.println("Cost of mercedes with leather seats: " + mercedes.computeCost());
    mercedes = new SunRoofDecorator(mercedes);
    System.out.println(
        "Cost of mercedes with sun roof and leather seats: " + mercedes.computeCost());

  }
}
