package and.digital.iLab.Design.patterns.interpreter.boyd;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HasOperator implements Expression {

    @Override
    public boolean interpreter(String context) {
        Pattern pattern = Pattern.compile("[+-/*]+");
        Matcher matcher = pattern.matcher(context);
        return matcher.find();
    }
}
