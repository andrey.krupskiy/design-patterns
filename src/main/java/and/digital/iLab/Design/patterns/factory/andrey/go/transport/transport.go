package transport

type Transport interface {
	MakeSound() string
}
