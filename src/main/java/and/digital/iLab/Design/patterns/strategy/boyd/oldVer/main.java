package and.digital.iLab.Design.patterns.strategy.boyd.oldVer;

public class main {
    public static void main(String[] args) {
        StrategyUser strategyUser = new StrategyUser();
        StrategyA strategyA = new StrategyA();
        StrategyB strategyB = new StrategyB();

        strategyUser.useStrategy(strategyA);
        strategyUser.useStrategy(strategyB);
    }
}
