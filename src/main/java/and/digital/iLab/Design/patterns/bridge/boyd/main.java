package and.digital.iLab.Design.patterns.bridge.boyd;

public class main {
    public static void main(String[] args) {
        Button button = new Button(new RedUiItem());
        button.draw();
    }
}
