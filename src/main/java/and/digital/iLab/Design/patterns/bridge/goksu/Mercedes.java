package and.digital.iLab.Design.patterns.bridge.goksu;

/**
 * @author goksu
 * @created 17/06/2022
 * MercedesE
 **/

public class Mercedes extends Car {

  public Mercedes(Colour colour) {
    super(colour);
  }

  public void paint() {
    System.out.println("Mercedes: " + colour.getColour());
  }
}
