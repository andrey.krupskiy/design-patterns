package and.digital.iLab.Design.patterns.factory.goksu;

/**
 * @author goksu
 * @created 17/06/2022
 * BMW
 **/

public class BMW1 extends Car {

  public BMW1(String model) {
    super(model);
  }
}
