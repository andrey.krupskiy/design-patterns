package main

import "fmt"

type TransportFactory interface {
	CreateTransport(brand string) Transport
}

func CreateFactory(factoryType string) (TransportFactory, error) {
	switch factoryType {
	case "car":
		return ModernCarFactory{}, nil
	default:
		return nil, fmt.Errorf("Failed to create a transport factory")
	}

}

type ModernCarFactory struct{}

func (f ModernCarFactory) CreateTransport(brand string) Transport {
	return NewCar(brand, Modern)
}
