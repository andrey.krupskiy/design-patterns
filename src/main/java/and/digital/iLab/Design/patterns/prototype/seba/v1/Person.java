package and.digital.iLab.Design.patterns.prototype.seba.v1;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.StringJoiner;

public class Person {

    private String name;
    private Address address;

    @JsonCreator
    public Person(@JsonProperty(value = "name") String name,@JsonProperty(value = "address") Address address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Person.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("address=" + address)
                .toString();
    }

    @Override
    public Person clone() {
        ObjectMapper objectMapper = new ObjectMapper();

        try {

            String otherAsString = objectMapper.writeValueAsString(this);
            return objectMapper.readValue(otherAsString, Person.class);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

}
