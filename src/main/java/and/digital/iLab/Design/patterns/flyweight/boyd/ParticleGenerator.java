package and.digital.iLab.Design.patterns.flyweight.boyd;

import java.util.ArrayList;
import java.util.List;

public class ParticleGenerator {
    public List<Particle> particleList = new ArrayList<>();

    public void CreateParticle(int x, int y, int lifetime, String name, int size) {
        ParticleType type = ParticleFactory.getParticleType(name, size);
        Particle particle = new Particle(1, 25,1,  type);
        particleList.add(particle);
    }
}
