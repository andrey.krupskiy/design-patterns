package and.digital.iLab.Design.patterns.facade.goksu;

/**
 * @author goksu
 * @created 24/06/2022
 * ProcessController
 **/

public class ProcessController {

  public void startA(){
    System.out.println("startA");
  }
  public void startE(){
    System.out.println("startE");
  }
  public void startB(){
    System.out.println("startB");
  }
  public void startD(){
    System.out.println("startD");
  }
  public void startC(){
    System.out.println("startC");
  }

  public void stopA(){
    System.out.println("stopA");
  }
  public void stopE(){
    System.out.println("stopE");
  }
  public void stopB(){
    System.out.println("stopB");
  }
  public void stopD(){
    System.out.println("stopD");
  }
  public void stopC(){
    System.out.println("stopC");
  }

}
