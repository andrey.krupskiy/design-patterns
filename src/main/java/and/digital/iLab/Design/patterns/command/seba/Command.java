package and.digital.iLab.Design.patterns.command.seba;



public interface Command <Tin, Tout> {

    public Tout execute(Tin tin);

}
