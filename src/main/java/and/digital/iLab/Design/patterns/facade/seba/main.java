package and.digital.iLab.Design.patterns.facade.seba;

import java.time.LocalDate;

public class main {

  public static void main(String[] args) {

    BicycleRentingCompany bicycleRentingCompany = new BicycleRentingCompany();

    // approach without facade
    BicycleFrame bicycleFrame = new BicycleFrame(22, 4, BicycleType.ELECTRIC);
    Owner owner = new Owner("ownerName", LocalDate.now());
    Bicycle bicycle = new Bicycle(BreakingType.HANDLE_BREAKS, bicycleFrame, owner);

    bicycleRentingCompany.rentOutBicycle(bicycle);

    // approach with facade
    bicycleRentingCompany.rentOutBicycle("handle breaks", 22, 4, "electric", "ownerName", LocalDate.now());
  }

}
