package and.digital.iLab.Design.patterns.factory.andrey.java.boat;

import and.digital.iLab.Design.patterns.factory.andrey.java.Transport;
import java.io.*;
import java.time.LocalDate;

public class Boat implements Transport {
  private String brand;
  private LocalDate productionDate;
  private int PeopleCapacity;

  public Boat(String brand) {
    this.brand = brand;
    this.productionDate = LocalDate.now();
    this.PeopleCapacity = 1;
  }

  public Boat WithPeopleCapacity(int capacity) throws Exception {
    int MAX_CAPACITY = 10;
    if (capacity <= 0) {
      throw new RuntimeException("Capacity can't be less than 0.");
    }

    if (capacity > 10) {
      throw new RuntimeException("We don't make boats this big");
    }

    this.PeopleCapacity = capacity;
    return this;
  }

  public String MakeSound() {
    return String.format(
        "BRRRRRRRRR I'm a %s boat, I was built on %s and I have capacity of %s people",
        this.brand, this.productionDate, this.PeopleCapacity);
  }
}
