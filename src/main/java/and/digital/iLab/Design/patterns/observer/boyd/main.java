package and.digital.iLab.Design.patterns.observer.boyd;

public class main {
    public static void main(String[] args) {
        Event event1 = new Event();
        Event event2 = new Event();
        Listener listener = new Listener();

        event1.addListener(listener);
        event2.addListener(listener);

        event1.setNewState("Hello");
        event2.setNewState("World!");

        listener.printEvents();
    }
}
