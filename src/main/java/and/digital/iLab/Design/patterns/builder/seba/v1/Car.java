package and.digital.iLab.Design.patterns.builder.seba.v1;

public class Car {

    private String brand;
    private int numOfWheels;
    private int doors;
    private int wheelSize;
    private boolean brandNew;
    private int numOfSeats;
    private boolean convertible;
    private int maxSpeed;

    public Car() {
    }

    public Car(String brand, int numOfWheels, int doors, int wheelSize, boolean brandNew, int numOfSeats, boolean convertible, int maxSpeed) {
        this.brand = brand;
        this.numOfWheels = numOfWheels;
        this.doors = doors;
        this.wheelSize = wheelSize;
        this.brandNew = brandNew;
        this.numOfSeats = numOfSeats;
        this.convertible = convertible;
        this.maxSpeed = maxSpeed;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getNumOfWheels() {
        return numOfWheels;
    }

    public void setNumOfWheels(int numOfWheels) {
        this.numOfWheels = numOfWheels;
    }

    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    public int getWheelSize() {
        return wheelSize;
    }

    public void setWheelSize(int wheelSize) {
        this.wheelSize = wheelSize;
    }

    public boolean isBrandNew() {
        return brandNew;
    }

    public void setBrandNew(boolean brandNew) {
        this.brandNew = brandNew;
    }

    public int getNumOfSeats() {
        return numOfSeats;
    }

    public void setNumOfSeats(int numOfSeats) {
        this.numOfSeats = numOfSeats;
    }

    public boolean isConvertible() {
        return convertible;
    }

    public void setConvertible(boolean convertible) {
        this.convertible = convertible;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }
}
