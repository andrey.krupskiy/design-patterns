package and.digital.iLab.Design.patterns.composite.goksu;

/**
 * @author goksu
 * @created 17/06/2022
 * Car
 **/

public interface Car {
  void printModel();
}
