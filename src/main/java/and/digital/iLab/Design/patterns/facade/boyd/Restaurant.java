package and.digital.iLab.Design.patterns.facade.boyd;

public class Restaurant
{
    public void TakeOrder(String food, int tableNr) {
        System.out.println("Taken a order for table " + tableNr + " they want " + food);
    }

    public void CookOrder(String food) {
        System.out.println("Cooked a " + food);
    }

    public void TakeOutOrder(String food, int tableNr) {
        System.out.println("Bringing the " + food + " to table " + tableNr);
    }
}
