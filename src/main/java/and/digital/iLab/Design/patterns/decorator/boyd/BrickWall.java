package and.digital.iLab.Design.patterns.decorator.boyd;

public class BrickWall implements Wall
{

    @Override
    public String buildWall() {
        return "Built a brick wall";
    }
}
