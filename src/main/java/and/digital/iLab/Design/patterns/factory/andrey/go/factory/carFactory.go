package factory

import (
	"factory/car"
	"factory/transport"
)

type CarFactory struct{}

func NewCarFactory() CarFactory { return CarFactory{} }

func (cf *CarFactory) CreateTransport(brand string) transport.Transport {
	return car.New(brand)
}
