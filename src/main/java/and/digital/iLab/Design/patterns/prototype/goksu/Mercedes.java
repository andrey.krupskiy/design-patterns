package and.digital.iLab.Design.patterns.prototype.goksu;

/**
 * @author goksu
 * @created 17/06/2022
 * MercedesS
 **/

public class Mercedes extends Car {
  private String year;
  private Window window;

  public Mercedes(String model) {
    super(model);
  }

  public Mercedes(String model, String year, Window window) {
    super(model);
    this.year = year;
    this.window = window;
  }

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public Window getWindow() {
    return window;
  }

  public void setWindow(Window window) {
    this.window = window;
  }

  public Car copy() {
    Mercedes mercedes = new Mercedes(super.getModel());
    mercedes.setYear(this.getYear());
    mercedes.setWindow(new Window(this.getWindow().getWindowType()));
    return new Mercedes(super.getModel(), year, new Window(window.getWindowType()));
  }

  @Override
  public String toString() {
    return "Mercedes{" +
        "model='" + super.getModel() + '\'' +
        "year='" + year + '\'' +
        ", window=" + window +
        '}';
  }
}
