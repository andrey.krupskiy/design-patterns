package and.digital.iLab.Design.patterns.interpreter.boyd;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IsOnlyNums implements Expression {

    HasOperator hasOperator = new HasOperator();

    @Override
    public boolean interpreter(String context) {
        Pattern pattern = Pattern.compile("[a-zA-Z]+" );
        Matcher matcher = pattern.matcher(context);
        if(matcher.find()) return false;
        return hasOperator.interpreter(context);

    }
}

