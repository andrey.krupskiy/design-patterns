package and.digital.iLab.Design.patterns.adapter.goksu;

import java.util.Enumeration;
import java.util.Iterator;

/**
 * Adapter class
 * @author goksu
 * @created 22/06/2022
 * EnumerationIterator
 **/

public class EnumerationIterator implements Iterator {
  private Enumeration enumeration;

  public EnumerationIterator(Enumeration enumeration) {
    this.enumeration = enumeration;
  }

  @Override
  public boolean hasNext() {
    return enumeration.hasMoreElements();
  }

  @Override
  public Object next() {
    return enumeration.nextElement();
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException();
  }
}
