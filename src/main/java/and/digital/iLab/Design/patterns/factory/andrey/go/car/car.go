package car

import (
	"fmt"
	"time"
)

type Car struct {
	brand          string
	productionDate time.Time
}

func New(brand string) Car {
	return Car{brand: brand,
		productionDate: time.Now(),
	}
}

func (c Car) MakeSound() string {
	return fmt.Sprintf("Honk-honk, I'm a %v", c.brand)
}
