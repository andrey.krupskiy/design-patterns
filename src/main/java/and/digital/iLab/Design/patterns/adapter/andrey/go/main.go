package main

import "fmt"

func main() {
	roundHole := NewRoundHole(5)
	roundPeg := NewRoundPeg(5)

	if roundHole.Fits(roundPeg) {
		fmt.Println("Round hole fits round peg")
	}

	smallSquarePeg := NewSquarePeg(2)
	largeSquarePeg := NewSquarePeg(20)

	smallSquarePegAdapter := NewSquarePegAdapter(smallSquarePeg)
	largeSquarePegAdapter := NewSquarePegAdapter(largeSquarePeg)

	if roundHole.Fits(smallSquarePegAdapter) {
		fmt.Println("Hole fits small square peg")
	}

	if !roundHole.Fits(largeSquarePegAdapter) {
		fmt.Println("Hole does not fit large square peg")
	}
}
