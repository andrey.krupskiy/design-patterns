package and.digital.iLab.Design.patterns.abstractFactory.goksu;

/**
 * @author goksu
 * @created 20/06/2022
 * MercedesSPartsFactory
 **/

public class MercedesSPartsFactory implements CarPartsFactory {
  @Override
  public Window createWindow() {
    return new WindowS();
  }

  @Override
  public Wheel createWheel() {
    return new WhellS();
  }

  @Override
  public SteeringWheel createSteeringWheel() {
    return new SteeringWheelS();
  }
}
