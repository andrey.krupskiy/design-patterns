package and.digital.iLab.Design.patterns.flyweight.goksu;

/**
 * @author goksu
 * @created 27/06/2022
 * main
 **/

public class main {
  public static void main(String[] args) {

    CarFactory carFactory = new CarFactory();
    carFactory.getCar("red").printCar();

    // Same objects
    carFactory.getCar("black").printCar();
    carFactory.getCar("black").printCar();

  }
}
