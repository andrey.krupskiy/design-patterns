package and.digital.iLab.Design.patterns.command.boyd;

public class Car {

    private String carBrand;

    public Car(String carBrand) {
        this.carBrand = carBrand;
    }

    public void Operate(Command command) {
        command.execute();
    }
}
