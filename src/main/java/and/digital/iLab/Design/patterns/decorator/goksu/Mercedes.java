package and.digital.iLab.Design.patterns.decorator.goksu;


/**
 * @author goksu
 * @created 17/06/2022
 * MercedesE
 **/

public class Mercedes implements Car {

  @Override
  public int computeCost() {
    return 100;
  }
}
