package main

import (
	"factory/factory"
	"fmt"
)

func main() {
	fmt.Println("Starting car building...")

	cf := factory.NewCarFactory()
	tr := cf.CreateTransport("Fiat")
	fmt.Println(tr.MakeSound())

}
