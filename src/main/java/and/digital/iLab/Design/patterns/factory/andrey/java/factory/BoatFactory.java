package and.digital.iLab.Design.patterns.factory.andrey.java.factory;

import and.digital.iLab.Design.patterns.factory.andrey.java.boat.Boat;

public class BoatFactory {
  public Boat createTransport(String brand) {
    return new Boat(brand);
  }
}
