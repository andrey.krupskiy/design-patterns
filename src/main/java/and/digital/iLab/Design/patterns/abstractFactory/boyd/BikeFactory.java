package and.digital.iLab.Design.patterns.abstractFactory.boyd;

public interface BikeFactory {
    Steer addSteer();
}
