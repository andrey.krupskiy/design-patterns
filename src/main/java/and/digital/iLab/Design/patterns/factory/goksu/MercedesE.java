package and.digital.iLab.Design.patterns.factory.goksu;

/**
 * @author goksu
 * @created 17/06/2022
 * MercedesE
 **/

public class MercedesE extends Car {

  public MercedesE(String model) {
    super(model);
  }
}
