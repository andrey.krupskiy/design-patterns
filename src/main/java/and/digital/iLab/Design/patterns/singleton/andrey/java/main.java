package and.digital.iLab.Design.patterns.singleton.andrey.java;

public class main {
    public static void main(String[] args) {
        DBClient c1 = DBClient.getClient("FOO");
        DBClient c2 = DBClient.getClient("BAR");

        System.out.println(c1.createdAt + " " +  c1.database);
        System.out.println(c2.createdAt + " " +  c2.database);
    }
}
