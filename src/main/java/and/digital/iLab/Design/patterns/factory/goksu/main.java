package and.digital.iLab.Design.patterns.factory.goksu;

/**
 * @author goksu
 * @created 17/06/2022
 * main
 **/

public class main {

  public static void main(String[] args) {

    CarFactory mercedesFactory = new MercedesFactory();
    Car car = mercedesFactory.createCar("e200");
    car.sell();

    car = mercedesFactory.createCar("s200");
    car.sell();

    CarFactory bmwFactory = new BMWFactory();
    car = bmwFactory.createCar("bmw1");
    car.sell();
  }
}
