package and.digital.iLab.Design.patterns.templateMethod.boyd;

public class PhysicalShop extends Shop {
    @Override
    public void sellItem() {
        System.out.println("Customer paid with cash");
        super.sellItem();
    }

    @Override
    public void deliver() {
        System.out.println("Put item in a bag");
        System.out.println("Gave the bag to the customer");
    }
}
