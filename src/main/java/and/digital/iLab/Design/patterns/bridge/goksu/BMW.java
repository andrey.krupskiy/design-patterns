package and.digital.iLab.Design.patterns.bridge.goksu;


/**
 * @author goksu
 * @created 17/06/2022
 * BMW
 **/

public class BMW extends Car {

  public BMW(Colour colour) {
    super(colour);
  }

  public void paint() {
    System.out.println("BMW: " + colour.getColour());
  }
}
