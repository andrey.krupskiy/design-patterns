package and.digital.iLab.Design.patterns.state.boyd.states;

import and.digital.iLab.Design.patterns.state.boyd.EnemyNPC;

public class SearchState extends State {
    SearchState(EnemyNPC enemyNPC) {
        super(enemyNPC);
    }

    @Override
    public void onIdle() {
        enemyNPC.setState(new IdleState(enemyNPC));
        enemyNPC.setAggressed(false);
        enemyNPC.setCurrentAction("Could not find anyone, back to idling");

    }

    @Override
    public void onSearching() {
        System.out.println("Already searching!");
    }

    @Override
    public void onChasing() {
        enemyNPC.setState(new ChaseState(enemyNPC));
        enemyNPC.setAggressed(true);
        enemyNPC.setCurrentAction("Found target, Chasing!");
    }


}
