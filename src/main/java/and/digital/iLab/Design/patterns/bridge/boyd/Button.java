package and.digital.iLab.Design.patterns.bridge.boyd;

public class Button extends UiItem{

    ColorUiItem colorUiItem;

    public Button(ColorUiItem colorUiItem) {
        this.colorUiItem = colorUiItem;
    }
    @Override
    public void draw() {
        System.out.println("Drew a button" + colorUiItem.setColor());
    }
}
