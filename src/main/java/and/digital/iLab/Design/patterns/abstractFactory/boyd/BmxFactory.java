package and.digital.iLab.Design.patterns.abstractFactory.boyd;

public class BmxFactory implements BikeFactory {
    public Steer addSteer() {
        return new BmxSteer();
    }
}
