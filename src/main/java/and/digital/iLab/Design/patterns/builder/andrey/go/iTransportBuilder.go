package main

type ITransportBuilder interface {
	Reset()
	GetResult() ITransport
	AddBrand(brand string)
	AddChassis(material Material)
	AddEngine(engine IEngine)
	AddRoof(material Material)
	AddControls(controls Controls)
}
