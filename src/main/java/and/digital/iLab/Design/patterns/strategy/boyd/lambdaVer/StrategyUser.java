package and.digital.iLab.Design.patterns.strategy.boyd.lambdaVer;

public class StrategyUser {
    public void useStrategy(Strategy strategy) {
        System.out.println(strategy.strategy());
    }
}
