package and.digital.iLab.Design.patterns.abstractFactory.goksu;


/**
 * @author goksu
 * @created 17/06/2022
 * MercedesFactory
 **/

public class MercedesFactory extends CarFactory {

  @Override
  public Car createCar(String model, CarPartsFactory carPartsFactory) {
    Car car;
    if (model.equals("e")) {
      car = new MercedesE(model, carPartsFactory);
    } else {
      car = new MercedesS(model, carPartsFactory);
    }
    car.assemble();
    return car;
  }
}
