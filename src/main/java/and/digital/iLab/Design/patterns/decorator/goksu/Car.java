package and.digital.iLab.Design.patterns.decorator.goksu;

/**
 * @author goksu
 * @created 17/06/2022
 * Car
 **/

public interface Car {
  int computeCost();
}
