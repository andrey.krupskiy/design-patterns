package and.digital.iLab.Design.patterns.singleton.seba;

public class main {

  public static void main(String[] args) throws Exception {

    Singleton initialValue = Singleton.getInstance("initial Value");

    System.out.println(initialValue);
    System.out.println(initialValue.getValue());

    Singleton secondValue = Singleton.getInstance("second Value");

    System.out.println(secondValue);
    System.out.println(secondValue.getValue());
  }

}
