package and.digital.iLab.Design.patterns.factory.boyd;

public class main {
    public static void main(String[] args) {
        ComputerFactory laptopFactory = new LaptopFactory();
        ComputerFactory desktopFactory = new DesktopFactory();

        Computer AsusLaptop = laptopFactory.createComputer("Asus");
        Computer MsiLpatop = laptopFactory.createComputer("MSI");
        AsusLaptop.getBrand();
        MsiLpatop.getBrand();

        Computer NZXTDesktop = desktopFactory.createComputer("NZXT");
        Computer AlienwareDesktop = desktopFactory.createComputer("Alienware");

        NZXTDesktop.getBrand();
        AlienwareDesktop.getBrand();
    }
}
