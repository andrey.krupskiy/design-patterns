package and.digital.iLab.Design.patterns.command.boyd;

public class Vehicle {

    public void Drive() {
        System.out.println("Driving");
    }

    public void Brake() {
        System.out.println("Breaking");
    }
}
