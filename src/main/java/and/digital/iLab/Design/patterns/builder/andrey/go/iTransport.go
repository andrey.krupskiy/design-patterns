package main

type ITransport interface {
	Transport() string
	MakeSound() string
}
