package and.digital.iLab.Design.patterns.builder.andrey.java;

public class Engine {
  private int volume;
  private int mileage;
  private boolean started;

  public Engine(int volume, int mileage) {
    this.volume = volume;
    this.mileage = mileage;
  }

  public void on() {
    this.started = true;
  }

  public void off() {
    this.started = false;
  }

  public boolean isStarted() {
    return this.started;
  }

  private String startedToString() {
    return this.started ? "running" : "idle";
  }

  public String toString() {
    return String.format(
        "Engine, %s volume, %s mileage. Engine is %s.",
        this.volume, this.mileage, startedToString());
  }
}
