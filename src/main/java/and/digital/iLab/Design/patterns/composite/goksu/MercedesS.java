package and.digital.iLab.Design.patterns.composite.goksu;

/**
 * @author goksu
 * @created 23/06/2022
 * MercedesS
 **/

public class MercedesS implements Car {

  private final String model;

  public MercedesS(String model) {
    this.model = model;
  }

  @Override
  public void printModel() {
    System.out.println("S" + model);
  }
}
