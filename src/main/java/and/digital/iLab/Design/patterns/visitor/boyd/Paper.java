package and.digital.iLab.Design.patterns.visitor.boyd;

public class Paper implements Writeable {

    private String text;

    @Override
    public void writeOn(String text) {
        this.text = text;
    }

    @Override
    public String accept(Visitor visitor) {
        return visitor.visitPaper(this);
    }

    public String getText() {
        return text;
    }
}
