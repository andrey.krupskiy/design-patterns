package main

type SquarePeg struct {
	width float32
}

func NewSquarePeg(width float32) SquarePeg {
	return SquarePeg{width}
}

func (sp SquarePeg) Width() float32 {
	return sp.width
}
