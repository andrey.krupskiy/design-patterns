package and.digital.iLab.Design.patterns.abstractFactory.andrey.java;

public class ModernCarFactory implements TransportFactory {
  public Transport CreateTransport(String brand) {
    return new Car(brand, TransportType.Modern);
  }
}
