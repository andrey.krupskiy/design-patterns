package main

type CarBuilder struct {
	brand    string
	chassis  Material
	engine   IEngine
	roof     Material
	controls Controls
}

func (b *CarBuilder) Reset() {
	b = &CarBuilder{}
}

func (b *CarBuilder) GetResult() ITransport {
	return NewCar(b.brand, b.chassis, b.engine, b.roof, b.controls)
}

func (b *CarBuilder) AddBrand(brand string) {
	b.brand = brand
}

func (b *CarBuilder) AddChassis(m Material) {
	b.chassis = m
}

func (b *CarBuilder) AddEngine(engine IEngine) {
	b.engine = engine
}

func (b *CarBuilder) AddRoof(roof Material) {
	b.roof = roof
}

func (b *CarBuilder) AddControls(c Controls) {
	b.controls = c
}
