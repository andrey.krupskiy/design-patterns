package and.digital.iLab.Design.patterns.builder.andrey.java;

public enum Material {
  metal,
  wood,
  glass,
  plastic,
}
