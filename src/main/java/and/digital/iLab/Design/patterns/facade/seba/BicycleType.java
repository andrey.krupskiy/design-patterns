package and.digital.iLab.Design.patterns.facade.seba;

import java.util.Arrays;

public enum BicycleType {

    ELECTRIC("electric"),
    STANDARD("standard");

    private String type;

    BicycleType(String type) {
        this.type = type;
    }

    public static BicycleType fromName(String name){
        return Arrays.stream(values()).filter(type -> type.getType().equalsIgnoreCase(name)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

    public String getType() {
        return type;
    }

}
