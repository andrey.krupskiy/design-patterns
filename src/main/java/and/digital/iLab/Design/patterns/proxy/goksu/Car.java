package and.digital.iLab.Design.patterns.proxy.goksu;

/**
 * @author goksu
 * @created 27/06/2022
 * Proxy
 **/

public interface Car {
  void assemble();
}
