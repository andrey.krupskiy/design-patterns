package and.digital.iLab.Design.patterns.factory.boyd;

public  abstract class Computer {
    private String brand;

    public Computer(String brand){
        this.brand = brand;
    }

    public void getBrand() {
        System.out.println("This computer is made by: " + brand);
    }


}
