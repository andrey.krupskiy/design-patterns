package and.digital.iLab.Design.patterns.flyweight.boyd;

public class ParticleType {
    String particleSprite;
    int size;

    public ParticleType(String particleSprite, int size) {
        this.particleSprite = particleSprite;
        this.size = size;
    }
}
