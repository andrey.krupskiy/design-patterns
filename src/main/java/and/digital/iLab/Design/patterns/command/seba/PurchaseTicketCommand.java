package and.digital.iLab.Design.patterns.command.seba;

import org.springframework.stereotype.Service;

@Service
public class PurchaseTicketCommand implements Command<PurchaseTicketRequest, String> {

    @Override
    public String execute(PurchaseTicketRequest in) {
        // here you do some logic depending on the command itself

        // ticketService.checkIfAvailableSeats(in.getFlightId(), in.getNumOfTravellers());
        // ticketService.calculateTotalPrice(in.getFlightPrice(), in.getNumOfTravellers());

        // ticketService.getBookingConfirmation(in.getFlightId(), in.getNumOfTravellers());

        return "some generate ID for the booking confirmation";
    }

}
