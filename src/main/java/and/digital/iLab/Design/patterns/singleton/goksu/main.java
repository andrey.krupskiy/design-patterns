package and.digital.iLab.Design.patterns.singleton.goksu;

/**
 * Singleton pattern with Enum
 *
 * @author goksu
 * @created 22/06/2022
 * main
 **/

public class main {
  public static void main(String[] args) {

    System.out.println(Singleton.INSTANCE.getValue());
    Singleton.INSTANCE.addOne();
    System.out.println(Singleton.INSTANCE.getValue());

  }
}
