package and.digital.iLab.Design.patterns.strategy.boyd.oldVer;

public class StrategyA implements Strategy {
    @Override
    public String useStrategy() {
        return "Now using strategyA";
    }
}
