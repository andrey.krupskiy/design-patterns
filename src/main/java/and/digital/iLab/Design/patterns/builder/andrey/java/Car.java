package and.digital.iLab.Design.patterns.builder.andrey.java;

public final class Car implements Transport {
  private final String brand;
  private final Material chassis;
  private final Engine engine;
  private final Material roof;
  private final Controls controls;

  public Car(String brand, Material chassis, Engine engine, Material roof, Controls controls) {
    this.brand = brand;
    this.chassis = chassis;
    this.engine = engine;
    this.roof = roof;
    this.controls = controls;
  }

  public void transport() {
    System.out.println("Vroom-vroom, you have arrived at your destination.");
  }

  public String makeSound() {
    return String.format("Honk-honk, I'm a %s car", this.brand);
  }

  public String toString() {
    return String.format(
        "%s car with %s chassis, %s engine, %s roof and %s controls",
        this.brand, this.chassis, this.engine, this.roof, this.controls);
  }
}
