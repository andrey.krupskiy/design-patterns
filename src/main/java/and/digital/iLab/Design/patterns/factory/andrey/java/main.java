package and.digital.iLab.Design.patterns.factory.andrey.java;

import and.digital.iLab.Design.patterns.factory.andrey.java.boat.Boat;
import and.digital.iLab.Design.patterns.factory.andrey.java.factory.BoatFactory;
import and.digital.iLab.Design.patterns.factory.andrey.java.factory.CarFactory;
import java.io.*;

public class main {
  public static void main(String[] args) {
    System.out.println("Starting a program...");

    System.out.println("Creating cars...");
    CarFactory carFactory = new CarFactory();
    Transport bmwCar = carFactory.createTransport("Toyota");
    System.out.println(bmwCar.MakeSound());

    System.out.println("Creating boats...");
    BoatFactory boatFactory = new BoatFactory();
    try {
      Transport smallBoat = boatFactory.createTransport("Feadship").WithPeopleCapacity(5);
      System.out.println(smallBoat.MakeSound());
    } catch (Exception e) {
      System.out.println(e);
    }

    System.out.println("Creating a faulty boat...");
    try {
      Boat tooBigBoat = boatFactory.createTransport("Boaty").WithPeopleCapacity(100);
      System.out.println(tooBigBoat.MakeSound());
    } catch (Exception e) {
      System.out.println(e);
    }
  }
}
