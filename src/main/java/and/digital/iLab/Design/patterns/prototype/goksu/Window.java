package and.digital.iLab.Design.patterns.prototype.goksu;

/**
 * @author goksu
 * @created 21/06/2022
 * Window
 **/

public class Window {
  private String windowType;

  public String getWindowType() {
    return windowType;
  }

  public void setWindowType(String windowType) {
    this.windowType = windowType;
  }

  public Window(String windowType) {
    this.windowType = windowType;
  }

  @Override
  public String toString() {
    return "Window{" +
        "windowType='" + windowType + '\'' +
        '}';
  }
}
