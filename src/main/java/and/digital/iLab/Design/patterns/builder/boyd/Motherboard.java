package and.digital.iLab.Design.patterns.builder.boyd;

public class Motherboard {
    private String brand;
    private Sockets socket;

    public Motherboard(String brand, Sockets socket) {
        this.brand = brand;
        this.socket = socket;
    }

    public String getBrand() {
        return brand;
    }

    public Sockets getSocket() {
        return socket;
    }

    @Override
    public String toString() {
        return "CPU: " + brand + ", " + socket;
    }
}
