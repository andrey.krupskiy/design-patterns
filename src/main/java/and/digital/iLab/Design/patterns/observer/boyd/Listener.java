package and.digital.iLab.Design.patterns.observer.boyd;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

public class Listener implements PropertyChangeListener {

    private List<String> events = new ArrayList<>();
    
    public void printEvents() {
       events.forEach(System.out::println);
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        events.add((String) event.getNewValue());
    }
}
