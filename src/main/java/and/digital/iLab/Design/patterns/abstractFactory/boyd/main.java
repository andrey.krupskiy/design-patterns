package and.digital.iLab.Design.patterns.abstractFactory.boyd;

public class main {
    public static void main(String[] args) {
        BikeFactory factory;
        String bike = "BMX";
        if(bike.equals("Ebike")) {
            factory = new EBikeFactory();
        } else {
            factory = new BmxFactory();
;        }
        Steer steer = factory.addSteer();
        steer.getSteer();
    }
}
