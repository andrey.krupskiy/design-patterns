package and.digital.iLab.Design.patterns.singleton.boyd;

public class Singleton {

    private static Singleton singleton;

    private int counter;

    private Singleton() {}

    public static synchronized Singleton getInstance() {
        if(singleton == null) {
            singleton = new Singleton();
            System.out.println();
        }
        return singleton;
    }

    public void addCount() {
        counter++;
        System.out.println(counter);
    }
}
