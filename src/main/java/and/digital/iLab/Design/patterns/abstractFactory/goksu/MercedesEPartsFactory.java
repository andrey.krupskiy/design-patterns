package and.digital.iLab.Design.patterns.abstractFactory.goksu;

/**
 * @author goksu
 * @created 20/06/2022
 * MercedesEPartsFactory
 **/

public class MercedesEPartsFactory implements CarPartsFactory {
  @Override
  public Window createWindow() {
    return new WindowE();
  }

  @Override
  public Wheel createWheel() {
    return new WheelE();
  }

  @Override
  public SteeringWheel createSteeringWheel() {
    return new SteeringWheelE();
  }
}
