package and.digital.iLab.Design.patterns.flyweight.goksu;

import java.util.HashMap;
import java.util.Map;

/**
 * @author goksu
 * @created 27/06/2022
 * CarFactory
 **/

public class CarFactory {
  private static final Map<String, Car> carMap = new HashMap();


  public Car getCar(String colour){
    return carMap.computeIfAbsent(colour, car -> new Mercedes(colour));
  }
}
