package and.digital.iLab.Design.patterns.prototype.goksu;

/**
 * @author goksu
 * @created 21/06/2022
 * main
 **/

public class main {
  public static void main(String[] args) {
    Car car = new Mercedes("e200", "2022", new Window("WindowE"));
    Car carCopy = car.copy();

    System.out.println("Before Update");
    System.out.println(car);
    System.out.println(carCopy);

    carCopy.setModel("s200");
    ((Mercedes) carCopy).getWindow().setWindowType("WindowS");

    System.out.println("After Update");
    System.out.println(car);
    System.out.println(carCopy);
  }
}
