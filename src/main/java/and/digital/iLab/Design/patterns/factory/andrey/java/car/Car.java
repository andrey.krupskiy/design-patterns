package and.digital.iLab.Design.patterns.factory.andrey.java.car;

import and.digital.iLab.Design.patterns.factory.andrey.java.Transport;
import java.io.*;
import java.time.LocalDate;

public class Car implements Transport {
  private String brand;
  private LocalDate productionDate;

  public Car(String brand) {
    this.brand = brand;
    this.productionDate = LocalDate.now();
  }

  public String MakeSound() {
    return String.format("Honk-honk, I'm %s and was built on %s", this.brand, this.productionDate);
  }
}
