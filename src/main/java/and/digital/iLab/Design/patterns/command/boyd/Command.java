package and.digital.iLab.Design.patterns.command.boyd;

public interface Command {

    public void execute();
}
