package and.digital.iLab.Design.patterns.singleton.andrey.java;

import java.sql.Time;
import java.time.LocalDate;
import java.util.Date;

public class DBClient {

    private static DBClient client;
    public LocalDate createdAt;
    public String database;

    private DBClient(String dbstring) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
       this.createdAt = LocalDate.now();
       this.database = dbstring;
    }

    public static DBClient getClient(String dbstring) {
        if (client == null) {
            client = new DBClient(dbstring);
        }
        return client;
    }

    public String makeDBCall() {
        return "beep-boop";
    }
}
