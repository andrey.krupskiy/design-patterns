package and.digital.iLab.Design.patterns.command.boyd;


public class BrakeCommand implements Command {

    private Vehicle vehicle;

    public BrakeCommand(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public void execute() {
        vehicle.Brake();
    }

}
