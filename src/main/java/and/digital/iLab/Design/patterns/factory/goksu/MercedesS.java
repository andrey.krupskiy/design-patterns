package and.digital.iLab.Design.patterns.factory.goksu;

/**
 * @author goksu
 * @created 17/06/2022
 * MercedesS
 **/

public class MercedesS extends Car{

  public MercedesS(String model) {
    super(model);
  }
}
