package main

import (
	"fmt"
	"time"
)

type TimeEra uint

const (
	Vintage TimeEra = iota
	Modern
)

type Car struct {
	brand          string
	carEra         TimeEra
	productionDate time.Time
}

func NewCar(brand string, carEra TimeEra) Car {
	return Car{
		brand,
		carEra,
		time.Now(),
	}
}

func (c Car) MakeSound() string {
	return fmt.Sprintf("I'm a %v %v car and I was made on %v", c.carEra, c.brand, c.productionDate)
}
