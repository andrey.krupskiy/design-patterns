package and.digital.iLab.Design.patterns.decorator.goksu;

/**
 * @author goksu
 * @created 23/06/2022
 * SunRoofDecorator
 **/

public class SunRoofDecorator extends MercedesDecorator {
  private final Car car;

  public SunRoofDecorator(Car car) {
    this.car = car;
  }

  @Override
  public int computeCost() {
    return car.computeCost() + 20;
  }
}
