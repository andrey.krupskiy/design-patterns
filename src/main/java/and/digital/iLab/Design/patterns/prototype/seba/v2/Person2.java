package and.digital.iLab.Design.patterns.prototype.seba.v2;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;

import java.util.StringJoiner;

public class Person2 {

    private String name;
    private Address2 address2;

    public Person2(String name, Address2 address2) {
        this.name = name;
        this.address2 = address2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address2 getAddress() {
        return address2;
    }

    public void setAddress(Address2 address2) {
        this.address2 = address2;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Person2.class.getSimpleName() + "[", "]")
                .add("name2='" + name + "'")
                .add("address2=" + address2)
                .toString();
    }

    public static Person2 clone(Person2 other) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new ParameterNamesModule(JsonCreator.Mode.PROPERTIES));

        try {

            String otherAsString = objectMapper.writeValueAsString(other);
            return objectMapper.readValue(otherAsString, Person2.class);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

}
