package and.digital.iLab.Design.patterns.factory.goksu;

/**
 * @author goksu
 * @created 17/06/2022
 * Car
 **/

public abstract class Car {

  private String model;

  public Car(String model) {
    this.model = model;
  }

  public void sell() {
    System.out.println("Sold: " + this.model);
  }
}
