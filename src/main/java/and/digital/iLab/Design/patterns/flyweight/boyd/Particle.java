package and.digital.iLab.Design.patterns.flyweight.boyd;

public class Particle {
    private int x;
    private int y;
    private int lifetime;
    private ParticleType particleType;

    public Particle(int x, int y, int lifetime, ParticleType particleType) {
        this.x = x;
        this.y = y;
        this.lifetime = lifetime;
        this.particleType = particleType;
    }

    public void getParticle () {
        System.out.println("This is a particle on X: " + x + " Y: " + y + " and is alive for " + lifetime);
    }

}
