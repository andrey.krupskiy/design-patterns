package and.digital.iLab.Design.patterns.factory.seba;

import java.time.LocalDate;

public class main {

  public static void main(String[] args) {

      Car newTeslaCar = Car.createNewTeslaCar("Elon");

      Car usedBMWCar = Car.createUsedBMWCar("John", LocalDate.ofYearDay(2019, 50));

  }

}
