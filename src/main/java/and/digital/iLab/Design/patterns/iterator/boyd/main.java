package and.digital.iLab.Design.patterns.iterator.boyd;

public class main {

    public static void main(String[] args) {

        final Person joe = new Person("Joe", 42, true);
        final Person john = new Person("John", 102, false);
        final Person alex = new Person("Alex", 18, true);

        People people = new People(joe, john, alex);
        PeopleIterator peopleIterator = people.iterator();

        while (peopleIterator.hasNext()) {
            Person person = peopleIterator.next();
            System.out.println(person.getName());
        }
    }
}
