package and.digital.iLab.Design.patterns.factory.seba;

import java.time.LocalDate;

public class Car {

    private Brand brand;
    private String owner;
    private LocalDate productionYear;

    private Car(Brand brand, String owner, LocalDate productionYear) {
        this.brand = brand;
        this.owner = owner;
        this.productionYear = productionYear;
    }

    public static Car createNewTeslaCar(String owner){
        return new Car(Brand.TESLA, owner, LocalDate.now());
    }

    public static Car createUsedBMWCar(String owner, LocalDate productionYear){
        return new Car(Brand.BMW, owner, LocalDate.ofYearDay(productionYear.getYear(), productionYear.getDayOfYear()));
    }

}
