package and.digital.iLab.Design.patterns.decorator.goksu;

/**
 * @author goksu
 * @created 23/06/2022
 * LeatherSeat
 **/

public class LeatherSeatDecorator extends MercedesDecorator {
  private final Car car;

  public LeatherSeatDecorator(Car car) {
    this.car = car;
  }

  @Override
  public int computeCost() {
    return car.computeCost() + 10;
  }
}
