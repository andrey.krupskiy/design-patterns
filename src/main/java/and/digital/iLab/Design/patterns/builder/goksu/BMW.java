package and.digital.iLab.Design.patterns.builder.goksu;

/**
 * @author goksu
 * @created 21/06/2022
 * BMW
 **/

public class BMW extends Car{
  private final String logo;

  private BMW(Builder builder) {
    super(builder);
    this.logo = builder.logo;
  }

  @Override
  public String toString() {
    return "BMW{" +
        "logo='" + logo + '\'' +
        ", accessories=" + accessories +
        '}';
  }

  protected static class Builder extends Car.Builder<Builder> {
    private String logo;

    public Builder addLogoToWheel(String logo) {
      this.logo =  logo;
      return self();
    }

    @Override
    Builder self() {
      return this;
    }

    @Override
    public BMW build() {
      return new BMW(this);
    }
  }
}
