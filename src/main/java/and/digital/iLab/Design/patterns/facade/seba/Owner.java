package and.digital.iLab.Design.patterns.facade.seba;

import java.time.LocalDate;

public class Owner {

    private String ownerName;
    private LocalDate rentedSince;

    public Owner(String ownerName, LocalDate rentedSince) {
        this.ownerName = ownerName;
        this.rentedSince = rentedSince;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public LocalDate getRentedSince() {
        return rentedSince;
    }
}
