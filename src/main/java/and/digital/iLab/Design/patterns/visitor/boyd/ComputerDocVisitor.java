package and.digital.iLab.Design.patterns.visitor.boyd;

public class ComputerDocVisitor implements Visitor {

    public String convert(Writeable...writeables) {
        StringBuilder sb = new StringBuilder();
        for (Writeable writeable: writeables) {
            sb.append(writeable.accept(this) + '\n');
        }
        return sb.toString();
    }

    @Override
    public String visitPaper(Paper paper) {
        return "Converted following text to a computer Doc: " + paper.getText();
    }

    @Override
    public String visitChalkboard(Chalkboard chalkboard) {
        return "Converted following text to a computer Doc: " + chalkboard.getText();
    }
}
