package and.digital.iLab.Design.patterns.abstractFactory.goksu;

/**
 * Abstract Design Pattern Example
 * @author goksu
 * @created 20/06/2022
 * main
 **/

public class main {

  public static void main(String[] args) {

    CarPartsFactory mercedesEPartsFactory = new MercedesEPartsFactory();
    CarPartsFactory mercedesSPartsFactory = new MercedesSPartsFactory();

    CarFactory mercedesFactory = new MercedesFactory();
    Car car = mercedesFactory.createCar("e", mercedesEPartsFactory);
    System.out.println(car);

    car = mercedesFactory.createCar("s", mercedesSPartsFactory);
    System.out.println(car);

  }
}
