package and.digital.iLab.Design.patterns.adapter.goksu;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Reference : Head First Design Patterns book
 *
 * @author goksu
 * @created 22/06/2022
 * main
 **/

public class main {
  public static void main(String[] args) {
    List<String> list = Arrays.asList("1", "2", "3");
    EnumerationIterator enumerationIterator = new EnumerationIterator(Collections.enumeration(list));
    try {
      enumerationIterator.remove();
    } catch (UnsupportedOperationException e){
      System.out.println(e);
    }
    while (enumerationIterator.hasNext()){
      System.out.println(enumerationIterator.next());
    }
  }
}
