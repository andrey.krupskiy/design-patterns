package and.digital.iLab.Design.patterns.builder.goksu;

/**
 * Abstract Builder example
 * @author goksu
 * @created 21/06/2022
 * main
 **/

public class main {
  public static void main(String[] args) {

    Mercedes mercedes = new Mercedes.Builder("S200")
        .addAccessory("SteelRim")
        .addAccessory("SunRoof")
        .build();
    System.out.println(mercedes.toString());

    BMW bmw = new BMW.Builder()
        .addLogoToWheel("BMW wheel logo")
        .addAccessory("LedLight")
        .addAccessory("LeatherCouch")
        .build();

    System.out.println(bmw.toString());

  }
}
