package and.digital.iLab.Design.patterns.facade.seba;

import java.util.Arrays;

public enum BreakingType {

    HANDLE_BREAKS("handle breaks"),
    BACKWARDS_BREAKS("backwards breaks");

    private String type;

    BreakingType(String type) {
        this.type = type;
    }

    public static BreakingType fromName(String name){
        return Arrays.stream(values()).filter(type -> type.getType().equalsIgnoreCase(name)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

    public String getType() {
        return type;
    }
}
