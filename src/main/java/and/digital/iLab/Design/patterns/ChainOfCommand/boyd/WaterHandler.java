package and.digital.iLab.Design.patterns.ChainOfCommand.boyd;

public class WaterHandler extends DrinkHandler {
    public void drink(String type) {
        if (type.equals("Water")) {
            System.out.println("Drinking a water");
            return;
        }
        super.drink(type);
    }

}
