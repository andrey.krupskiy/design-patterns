package and.digital.iLab.Design.patterns.decorator.boyd;

public class WallWithWhitePaint implements Wall {

    Wall wall;

    public WallWithWhitePaint(Wall wall) {
        this.wall = wall;
    }

    @Override
    public String buildWall() {
        return wall.buildWall() + "  white paint on it";
    }
}
