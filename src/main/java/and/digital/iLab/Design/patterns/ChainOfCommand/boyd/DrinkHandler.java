package and.digital.iLab.Design.patterns.ChainOfCommand.boyd;

public abstract class DrinkHandler {

    private DrinkHandler next;

    public static DrinkHandler link(DrinkHandler first, DrinkHandler...chain) {
        DrinkHandler head = first;
        for (DrinkHandler nextInChain: chain) {
            head.next = nextInChain;
            head = nextInChain;
        }
        return first;
    }

    public void drink(String type) {
        if(next == null){
            return;
        }
        next.drink(type);
    }
}
