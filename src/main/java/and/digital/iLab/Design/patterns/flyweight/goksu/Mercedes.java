package and.digital.iLab.Design.patterns.flyweight.goksu;

/**
 * @author goksu
 * @created 27/06/2022
 * Mercedes
 **/

public class Mercedes implements Car {
  private String colour;

  public Mercedes(String colour) {
    this.colour = colour;
  }

  @Override
  public void printCar() {
    System.out.println("Reference : " + this +"- Colour: "+ this.colour);
  }
}
