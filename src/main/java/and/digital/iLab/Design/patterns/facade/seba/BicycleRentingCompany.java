package and.digital.iLab.Design.patterns.facade.seba;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class BicycleRentingCompany {

    private List<Bicycle> rentedBicycles = new ArrayList<>();

    public Bicycle rentOutBicycle(Bicycle bicycle){
        rentedBicycles.add(bicycle);
        return bicycle;
    }

    public Bicycle rentOutBicycle(String breakingType, int wheelSize, int numOfLights, String bicycleType,
                                  String ownerName, LocalDate rentedSince){

        Bicycle bicycleFacade = new Bicycle(breakingType, wheelSize, numOfLights, bicycleType, ownerName, rentedSince);
        rentedBicycles.add(bicycleFacade);
        return bicycleFacade;
    }

}
