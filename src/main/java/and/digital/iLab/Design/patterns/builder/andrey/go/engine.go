package main

import "fmt"

type IEngine interface {
	ExertPower() string
}

type genericEngine struct {
	volume  uint
	mileage uint
}

func (e genericEngine) String() string {
	return fmt.Sprintf("Engine with %v volume, %v mileage", e.volume, e.mileage)
}

type CityEngine struct {
	genericEngine
}

func NewCityEngine(volume, mileage uint) IEngine {
	return CityEngine{genericEngine{volume, mileage}}
}

func (e CityEngine) ExertPower() string {
	return fmt.Sprint("brrr")
}

func (e CityEngine) String() string {
	return fmt.Sprintf("City engine. %v", e.genericEngine)
}

type RaceEngine struct {
	genericEngine
}

func NewRaceEngine(volume, mileage uint) IEngine {
	return RaceEngine{genericEngine{volume, mileage}}
}

func (e RaceEngine) ExertPower() string {
	return fmt.Sprint("VRRRRROOOOOMMMMMMMM")
}

func (e RaceEngine) String() string {
	return fmt.Sprintf("Race engine. %v", e.genericEngine)
}
