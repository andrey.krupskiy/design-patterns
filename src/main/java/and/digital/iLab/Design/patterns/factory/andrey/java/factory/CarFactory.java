package and.digital.iLab.Design.patterns.factory.andrey.java.factory;

import and.digital.iLab.Design.patterns.factory.andrey.java.car.Car;

public class CarFactory extends Factory {
  public Car createTransport(String brand) {
    return new Car(brand);
  }
}
