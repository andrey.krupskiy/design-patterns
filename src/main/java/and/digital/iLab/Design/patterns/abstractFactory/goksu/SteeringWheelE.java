package and.digital.iLab.Design.patterns.abstractFactory.goksu;

/**
 * @author goksu
 * @created 20/06/2022
 * SteeringWheelE
 **/

public class SteeringWheelE implements SteeringWheel {
  @Override
  public String toString() {
    return "SteeringWheelE";
  }
}
