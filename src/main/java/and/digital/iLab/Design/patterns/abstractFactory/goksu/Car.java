package and.digital.iLab.Design.patterns.abstractFactory.goksu;

/**
 * @author goksu
 * @created 17/06/2022
 * Car
 **/

public abstract class Car {

  private String model;
  Window window;
  SteeringWheel steeringWheel;
  Wheel wheel;


  public Car(String model) {
    this.model = model;
  }

  abstract void assemble();

  @Override
  public String toString() {
    return "Car{" +
        "model='" + model + '\'' +
        ", window=" + window +
        ", steeringWheel=" + steeringWheel +
        ", wheel=" + wheel +
        '}';
  }
}
