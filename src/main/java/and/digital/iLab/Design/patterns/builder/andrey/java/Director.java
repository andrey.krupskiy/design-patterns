package and.digital.iLab.Design.patterns.builder.andrey.java;

public class Director {
  public void constructSportsCar(TransportBuilder builder) {
    builder.addBrand("Porsche");
    builder.addChassis(Material.plastic);
    builder.addEngine(new Engine(500, 0));
    builder.addRoof(Material.glass);
    builder.addControls(Controls.digital);
  }
}
