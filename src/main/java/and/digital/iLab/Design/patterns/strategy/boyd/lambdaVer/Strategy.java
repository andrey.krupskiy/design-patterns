package and.digital.iLab.Design.patterns.strategy.boyd.lambdaVer;

public interface Strategy {
    String strategy();
    
    Strategy strategyA = () -> "Using strategyA now";

    Strategy strategyB = () -> "Using strategyB now";

}
