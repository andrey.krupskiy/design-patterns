package and.digital.iLab.Design.patterns.abstractFactory.goksu;

/**
 * @author goksu
 * @created 20/06/2022
 * WindowE
 **/

public class WindowE implements Window {
  @Override
  public String toString() {
    return "WindowE";
  }
}
