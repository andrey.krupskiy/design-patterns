package main

import "math"

type SquarePegAgapter struct {
	sp SquarePeg
}

func NewSquarePegAdapter(sp SquarePeg) SquarePegAgapter {
	return SquarePegAgapter{sp}
}

func (a SquarePegAgapter) Radius() float32 {
	return a.sp.Width() * float32(math.Sqrt(2))
}
