package and.digital.iLab.Design.patterns.prototype.goksu;

/**
 * @author goksu
 * @created 17/06/2022
 * Car
 **/

public abstract class Car {

  private String model;

  public Car(String model) {
    this.model = model;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public abstract Car copy();
}
