package and.digital.iLab.Design.patterns.strategy.boyd.oldVer;

public class StrategyUser {
    public void useStrategy(Strategy strategy) {
        System.out.println(strategy.useStrategy());
    }
}
