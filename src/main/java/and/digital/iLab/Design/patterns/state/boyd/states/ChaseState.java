package and.digital.iLab.Design.patterns.state.boyd.states;

import and.digital.iLab.Design.patterns.state.boyd.EnemyNPC;

public class ChaseState extends State {
    ChaseState(EnemyNPC enemyNPC) {
        super(enemyNPC);
    }

    @Override
    public void onIdle() {
        enemyNPC.setState(new IdleState(enemyNPC));
        enemyNPC.setAggressed(false);
        enemyNPC.setCurrentAction("Target lost back to idling");
    }

    @Override
    public void onSearching() {
        enemyNPC.setState(new SearchState(enemyNPC));
        enemyNPC.setAggressed(false);
        enemyNPC.setCurrentAction("Lost the target back to searching");
    }

    @Override
    public void onChasing() {
        System.out.println("Already chasing!");
    }

}
