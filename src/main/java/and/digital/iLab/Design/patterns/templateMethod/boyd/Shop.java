package and.digital.iLab.Design.patterns.templateMethod.boyd;

public abstract class Shop {

    public void sellItem() {
        System.out.println("Received payment");
        System.out.println("Remove item from stock");
    }

    public void deliver() {};

    public void customerCycle() {
        sellItem();
        deliver();
    }


}
