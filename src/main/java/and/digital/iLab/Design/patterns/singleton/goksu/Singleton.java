package and.digital.iLab.Design.patterns.singleton.goksu;

/**
 * @author goksu
 * @created 22/06/2022
 * Singleton
 **/

public enum Singleton {
  INSTANCE(1);

  int value;

  Singleton(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }

  public void addOne() {
    value++;
  }

}
