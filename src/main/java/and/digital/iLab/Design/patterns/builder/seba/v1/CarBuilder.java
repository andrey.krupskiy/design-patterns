package and.digital.iLab.Design.patterns.builder.seba.v1;

public class CarBuilder {

    private Car car = new Car();

    public CarBuilder withBrand(String brand){
        car.setBrand(brand);
        return this;
    }

    public CarBuilder withWheels(int numOfWheels){
        car.setNumOfWheels(numOfWheels);
        return this;
    }

    public CarBuilder withDoors(int doors){
        car.setDoors(doors);
        return this;
    }

    public CarBuilder withWheelSize(int wheelSize){
        car.setWheelSize(wheelSize);
        return this;
    }

    public CarBuilder withBrandNew(boolean brandNew){
        car.setBrandNew(brandNew);
        return this;
    }

    public CarBuilder withNumOfSeats(int numOfSeats){
        car.setNumOfSeats(numOfSeats);
        return this;
    }

    public CarBuilder withConvertible(boolean convertible){
        car.setConvertible(convertible);
        return this;
    }

    public CarBuilder withMaxSpeed(int maxSpeed){
        car.setMaxSpeed(maxSpeed);
        return this;
    }

    public Car build(){
        return this.car;
    }


}
