package main

type RoundPeg interface {
	Radius() float32
}

type RoundPegImpl struct {
	radius float32
}

func NewRoundPeg(radius float32) RoundPegImpl {
	return RoundPegImpl{radius: radius}
}

func (rp RoundPegImpl) Radius() float32 {
	return rp.radius
}
