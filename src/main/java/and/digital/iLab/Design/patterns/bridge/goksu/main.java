package and.digital.iLab.Design.patterns.bridge.goksu;

/**
 * @author goksu
 * @created 22/06/2022
 * main
 **/

public class main {
  public static void main(String[] args) {
    Colour colour = new Red();
    Car redMercedes = new Mercedes(colour);
    redMercedes.paint();

    colour = new Blue();
    Car blueBMW = new BMW(colour);
    blueBMW.paint();
  }
}
