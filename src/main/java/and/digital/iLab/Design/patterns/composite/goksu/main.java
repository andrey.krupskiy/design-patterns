package and.digital.iLab.Design.patterns.composite.goksu;

/**
 * @author goksu
 * @created 22/06/2022
 * main
 **/

public class main {
  public static void main(String[] args) {
    Mercedes mercedes = new Mercedes();
    Mercedes mercedesS = new Mercedes();
    mercedesS.addCar(new MercedesS("200"));
    mercedesS.addCar(new MercedesS("300"));

    Mercedes mercedesE = new Mercedes();
    mercedesE.addCar(new MercedesE("200"));
    mercedesE.addCar(new MercedesE("400"));

    mercedes.addCar(mercedesS);
    mercedes.addCar(mercedesE);

    mercedes.printModel();

  }
}
