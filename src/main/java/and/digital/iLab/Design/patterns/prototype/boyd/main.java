package and.digital.iLab.Design.patterns.prototype.boyd;

public class main {
    public static void main(String[] args) {
        Room[] rooms = {new Room(100, "Bedroom")};
        House house = new House(150, rooms);

        House clonedHouse = house.clone();
        clonedHouse.getRooms()[0].setSize(200);

        System.out.println(house.getRooms()[0].getSize());
        System.out.println(clonedHouse.getRooms()[0].getSize());
    }
}
