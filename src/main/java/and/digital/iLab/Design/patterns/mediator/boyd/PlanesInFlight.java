package and.digital.iLab.Design.patterns.mediator.boyd;

import java.util.ArrayList;

public class PlanesInFlight {

  ArrayList<Plane> planes = new ArrayList<>();

  public void addPlane(Plane plane) {
    planes.add(plane);
  }

}
