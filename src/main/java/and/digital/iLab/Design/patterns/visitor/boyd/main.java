package and.digital.iLab.Design.patterns.visitor.boyd;

public class main {
    public static void main(String[] args) {
        Paper paper = new Paper();
        Chalkboard chalkboard = new Chalkboard();

        paper.writeOn("Hello");
        chalkboard.writeOn("World!");

        ComputerDocVisitor computerDocVisitor = new ComputerDocVisitor();
        System.out.println(computerDocVisitor.convert(paper, chalkboard));
    }
}
