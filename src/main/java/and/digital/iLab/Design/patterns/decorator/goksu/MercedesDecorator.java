package and.digital.iLab.Design.patterns.decorator.goksu;

/**
 * @author goksu
 * @created 23/06/2022
 * MercedesDecorator
 **/

public abstract class MercedesDecorator implements Car {

  public abstract int computeCost();
}
