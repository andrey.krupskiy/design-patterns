package and.digital.iLab.Design.patterns.visitor.boyd;

public interface Writeable {
    void writeOn(String text);
    String getText();

    String accept(Visitor visitor);
}
