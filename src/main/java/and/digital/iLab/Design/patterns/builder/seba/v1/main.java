package and.digital.iLab.Design.patterns.builder.seba.v1;

public class main {

  public static void main(String[] args) {

      CarBuilder carBuilder = new CarBuilder();

      Car tesla = carBuilder.withBrand("Tesla")
              .withConvertible(true)
              .withBrandNew(true)
              .withDoors(2)
              .withMaxSpeed(Integer.MAX_VALUE)
              .withWheels(4)
              .withNumOfSeats(5)
              .withWheelSize(21)
              .build();

  }
}
