package and.digital.iLab.Design.patterns.proxy.goksu;

/**
 * @author goksu
 * @created 27/06/2022
 * CarRealService
 **/

public class CarRealService implements Car{
  @Override
  public void assemble() {
    System.out.println("Assemble car!");
  }
}
