package and.digital.iLab.Design.patterns.facade.goksu;
/**
* @author goksu
* @created 24/06/2022
* main **/

public class main {
  public static void main(String[] args) {
    ProcessFacade processFacade = new ProcessFacade();
    processFacade.runStartProcessesInAlphabeticalOrder();
    processFacade.runStopProcessesInAlphabeticalOrder();

  }
}
