package and.digital.iLab.Design.patterns.composite.goksu;


/**
 * @author goksu
 * @created 17/06/2022
 * MercedesE
 **/

public class MercedesE implements Car {

  private final String model;

  public MercedesE(String model) {
    this.model = model;
  }

  @Override
  public void printModel() {
    System.out.println("E" + model);
  }
}
