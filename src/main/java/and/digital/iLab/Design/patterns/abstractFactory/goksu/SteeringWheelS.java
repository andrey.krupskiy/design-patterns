package and.digital.iLab.Design.patterns.abstractFactory.goksu;

/**
 * @author goksu
 * @created 20/06/2022
 * SteeringWheelS
 **/

public class SteeringWheelS implements SteeringWheel {
  @Override
  public String toString() {
    return "SteeringWheelS";
  }
}
