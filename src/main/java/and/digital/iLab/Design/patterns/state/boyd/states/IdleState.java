package and.digital.iLab.Design.patterns.state.boyd.states;

import and.digital.iLab.Design.patterns.state.boyd.EnemyNPC;

public class IdleState extends State{
    public IdleState(EnemyNPC enemyNPC) {
        super(enemyNPC);
    }

    @Override
    public void onIdle() {
        System.out.println("Already idling");
    }

    @Override
    public void onSearching() {
        enemyNPC.setState(new SearchState(enemyNPC));
        enemyNPC.setAggressed(false);
        enemyNPC.setCurrentAction("Searching...");
    }

    @Override
    public void onChasing() {
        enemyNPC.setState(new ChaseState(enemyNPC));
        enemyNPC.setAggressed(true);
        enemyNPC.setCurrentAction("Chasing");
    }
}
