package main

import "time"

type DBClient struct {
	CreatedAt time.Time
	DBString  string
}

var dbClient *DBClient

func newDBClient(dbstring string) DBClient {
	return DBClient{
		CreatedAt: time.Now(),
		DBString:  dbstring,
	}
}

func GetDBClient(dbstring string) *DBClient {
	if dbClient == nil {
		c := newDBClient(dbstring)
		dbClient = &c
	}
	return dbClient
}
