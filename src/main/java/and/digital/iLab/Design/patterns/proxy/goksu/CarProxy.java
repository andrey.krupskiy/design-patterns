package and.digital.iLab.Design.patterns.proxy.goksu;

/**
 * @author goksu
 * @created 27/06/2022
 * CarProxy
 **/

public class CarProxy implements Car{
  private static Car car;

  @Override
  public void assemble() {
    if(car == null){
      car = new CarRealService();
    }
    car.assemble();
  }
}
