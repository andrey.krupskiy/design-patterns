package and.digital.iLab.Design.patterns.abstractFactory.goksu;

/**
 * @author goksu
 * @created 20/06/2022
 * WhellS
 **/

public class WhellS implements Wheel {
  @Override
  public String toString() {
    return "WhellS";
  }
}
