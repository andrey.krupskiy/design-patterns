package and.digital.iLab.Design.patterns.iterator.boyd;

import java.util.Iterator;

public class PeopleIterator implements Iterator {

    private People people;
    private int index;

    public PeopleIterator(People people) {
        this.people = people;
        index = 0;
    }

    @Override
    public boolean hasNext() {
        Person[] persons = people.getPeople();
        return index < persons.length;
    }

    @Override
    public Person next() {
        Person[] persons = people.getPeople();
        if(hasNext()) {
            Person person = persons[index++];
            if (person.isAlive) {
                return person;
            }
            return next();
        }
        return null;
    }
}
