package and.digital.iLab.Design.patterns.command.boyd;

public class main {
    private static Vehicle vehicle = new Vehicle();
    public static void main(String[] args) {
        Car audi = new Car("Audi");
        Car tesla = new Car("Tesla");

        audi.Operate(new DriveCommand(vehicle));
        audi.Operate(new BrakeCommand(vehicle));
        tesla.Operate(new DriveCommand(vehicle));
    }
}
