package and.digital.iLab.Design.patterns.abstractFactory.andrey.java;

import java.time.LocalDate;

public class Car implements Transport {
  private String brand;
  private TransportType transportType;
  private LocalDate productionDate;

  public Car(String brand, TransportType type) {
    this.brand = brand;
    this.transportType = type;
    this.productionDate = LocalDate.now();
  }

  public String MakeSound() {
    return String.format(
        "Honk-honk, I'm %s %s and was built on %s",
        this.transportType, this.brand, this.productionDate);
  }
}
