package and.digital.iLab.Design.patterns.bridge.boyd;

public abstract class UiItem {
    public void draw() {
        System.out.println("Drew a UI item");
    }
}
