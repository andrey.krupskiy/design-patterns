package and.digital.iLab.Design.patterns.abstractFactory.boyd;

public class EBikeSteer implements Steer {
    @Override
    public void getSteer() {
        System.out.println("This is a E-Bike steer");
    }
}
