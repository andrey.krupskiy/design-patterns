package and.digital.iLab.Design.patterns.command.seba;

public class PurchaseTicketRequest {

    private String flightId;
    private int flightPrice;
    private int numOfTravellers;

    public static PurchaseTicketRequest from(String flightId, int flightPrice, int numOfTravellers){
        return new PurchaseTicketRequest(flightId, flightPrice, numOfTravellers);
    }

    private PurchaseTicketRequest(String flightId, int flightPrice, int numOfTravellers) {
        this.flightId = flightId;
        this.flightPrice = flightPrice;
        this.numOfTravellers = numOfTravellers;
    }

    public String getFlightId() {
        return flightId;
    }

    public int getFlightPrice() {
        return flightPrice;
    }

    public int getNumOfTravellers() {
        return numOfTravellers;
    }
}
