package and.digital.iLab.Design.patterns.memento.boyd;

public class main {
    public static void main(String[] args) {
        StringWriter stringWriter = new StringWriter("Hello");
        System.out.println(stringWriter.getString());
        stringWriter.setString("Hello World");
        System.out.println(stringWriter.getString());
        stringWriter.setString("tfypo");
        System.out.println(stringWriter.getString());
        stringWriter.undo();
        System.out.println(stringWriter.getString());
    }
}
