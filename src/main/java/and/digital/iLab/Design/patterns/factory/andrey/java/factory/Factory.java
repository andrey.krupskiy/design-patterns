package and.digital.iLab.Design.patterns.factory.andrey.java.factory;

import and.digital.iLab.Design.patterns.factory.andrey.java.car.Car;

public abstract class Factory {
  public abstract Car createTransport(String brand);
}
