package and.digital.iLab.Design.patterns.prototype.seba.v1;

public class main {

  public static void main(String[] args) {
      Address address1 = new Address("streetName1", 1);

      Person person1 = new Person("person1", address1);

      Person clone = person1.clone();
      clone.setName("changed");
      clone.getAddress().setStreet("changed streetName");

      address1.setNum(2);

    System.out.println(person1.toString());
    System.out.println(clone.toString());
  }

}
