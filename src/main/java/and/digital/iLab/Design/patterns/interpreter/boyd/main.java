package and.digital.iLab.Design.patterns.interpreter.boyd;

public class main {
    public static void main(String[] args) {
        String example1 = "1+1";
        String example2 = "ABC";
        String example3 = "100/A323";
        String example4 = "123";

        IsOnlyNums isOnlyNums = new IsOnlyNums();
        System.out.println(isOnlyNums.interpreter(example1));
        System.out.println(isOnlyNums.interpreter(example2));
        System.out.println(isOnlyNums.interpreter(example3));
        System.out.println(isOnlyNums.interpreter(example4));
    }
}
