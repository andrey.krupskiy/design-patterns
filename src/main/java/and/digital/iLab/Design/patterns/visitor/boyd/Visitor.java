package and.digital.iLab.Design.patterns.visitor.boyd;

public interface Visitor {
    String visitPaper(Paper paper);
    String visitChalkboard(Chalkboard chalkboard);
}
