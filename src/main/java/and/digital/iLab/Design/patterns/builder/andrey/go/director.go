package main

type Director struct{}

func (d Director) CreateSportsCar(b ITransportBuilder) {
	b.AddBrand("Porsche")
	b.AddChassis(plastic)
	b.AddEngine(NewRaceEngine(500, 0))
	b.AddRoof(glass)
	b.AddControls(digital)
}
