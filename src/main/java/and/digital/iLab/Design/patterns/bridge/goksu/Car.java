package and.digital.iLab.Design.patterns.bridge.goksu;

/**
 * @author goksu
 * @created 17/06/2022
 * Car
 **/

public abstract class Car {

  protected Colour colour;

  public Car(Colour colour) {
    this.colour = colour;
  }

  public abstract void paint();
}
