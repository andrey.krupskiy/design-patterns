package and.digital.iLab.Design.patterns.bridge.boyd;

public interface ColorUiItem {
    public String setColor();
}
