package and.digital.iLab.Design.patterns.decorator.boyd;

public class main
{
    public static void main(String[] args) {
        BrickWall wall = new BrickWall();
        WallWithWhitePaint whiteWall = new WallWithWhitePaint(wall);
        System.out.println(whiteWall.buildWall());
    }
}
