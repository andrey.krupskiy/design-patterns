package and.digital.iLab.Design.patterns.command.seba;

public class main {

  public static void main(String[] args) {

      // This should be injected via constructor in a controller
      PurchaseTicketCommand purchaseTicketCommand = new PurchaseTicketCommand();


      // This should be returned as a response in the controller
      String flightId = purchaseTicketCommand.execute(PurchaseTicketRequest.from("flightId", 500, 2));

  }

}
