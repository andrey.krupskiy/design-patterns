package and.digital.iLab.Design.patterns.factory.boyd;

public class LaptopFactory extends ComputerFactory{
    @Override
    public Computer createComputer(String supplier) {
        switch(supplier) {
            case "Asus":
                return new AsusLaptop(supplier);
            case "MSI":
                return new MSILaptop(supplier);
            default:
                return null;
        }
    }
}
