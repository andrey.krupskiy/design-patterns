package and.digital.iLab.Design.patterns.bridge.goksu;

/**
 * @author goksu
 * @created 22/06/2022
 * Red
 **/

public class Red implements Colour {
  @Override
  public String getColour() {
    return "Red";
  }
}
