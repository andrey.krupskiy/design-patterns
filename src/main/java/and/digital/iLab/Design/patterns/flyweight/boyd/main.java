package and.digital.iLab.Design.patterns.flyweight.boyd;

import java.util.Random;

public class main {
    public static void main(String[] args) {
        int particleCount = 1000000;
        long before = Runtime.getRuntime().freeMemory();
        ParticleGenerator particleGenerator = new ParticleGenerator();
        for (int i = 0; i < particleCount; i++) {
            int rand1 = (int) (Math.random() * 100);
            int rand2 = (int) (Math.random() * 100);
            particleGenerator.CreateParticle(rand1, rand2, 1, "Flame", 10);
        }
        long after = Runtime.getRuntime().freeMemory();

        System.out.println("Total ram used by particles: " + (before - after) /1024/1000);
    }
}
