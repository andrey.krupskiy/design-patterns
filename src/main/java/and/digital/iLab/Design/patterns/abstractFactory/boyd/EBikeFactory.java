package and.digital.iLab.Design.patterns.abstractFactory.boyd;

public class EBikeFactory implements BikeFactory {

    @Override
    public Steer addSteer() {
        return new EBikeSteer();
    }
}
