package and.digital.iLab.Design.patterns.state.boyd;

public class main {
    public static void main(String[] args) {
         EnemyNPC enemyNPC = new EnemyNPC();

         enemyNPC.getState().onIdle();

         enemyNPC.getState().onSearching();
        System.out.println(enemyNPC.getCurrentAction());

        enemyNPC.getState().onChasing();
        System.out.println(enemyNPC.getCurrentAction());

        enemyNPC.getState().onChasing();
        System.out.println(enemyNPC.isAggressed());
    }
}
