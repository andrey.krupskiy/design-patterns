package and.digital.iLab.Design.patterns.visitor.boyd;

public class Chalkboard  implements Writeable {
    private String text;

    @Override
    public void writeOn(String text) {
        this.text = text;
    }

    @Override
    public String accept(Visitor visitor) {
        return visitor.visitChalkboard(this);
    }

    public String getText() {
        return text;
    }
}
