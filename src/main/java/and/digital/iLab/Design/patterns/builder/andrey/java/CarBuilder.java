package and.digital.iLab.Design.patterns.builder.andrey.java;

public class CarBuilder implements TransportBuilder {
  private String brand;
  private Material chassis;
  private Engine engine;
  private Material roof;
  private Controls controls;

  public CarBuilder reset() {
    return new CarBuilder();
  }

  public Transport getResult() {
    return new Car(this.brand, this.chassis, this.engine, this.roof, this.controls);
  }

  public void addBrand(String brand) {
    this.brand = brand;
  }

  public void addChassis(Material material) {
    this.chassis = material;
  }

  public void addEngine(Engine engine) {
    this.engine = engine;
  }

  public void addRoof(Material material) {
    this.roof = material;
  }

  public void addControls(Controls controlsType) {
    this.controls = controlsType;
  }
}
