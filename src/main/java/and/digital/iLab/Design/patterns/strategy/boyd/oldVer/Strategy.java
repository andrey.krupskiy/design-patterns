package and.digital.iLab.Design.patterns.strategy.boyd.oldVer;

public interface Strategy {
    String useStrategy();
}
