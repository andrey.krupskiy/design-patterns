package and.digital.iLab.Design.patterns.factory.boyd;

public class AlienwareDesktop extends Computer {

    public AlienwareDesktop(String brand) {
        super(brand);
    }
    @Override
    public void getBrand() {
        System.out.println("This is a overpriced PC");
    }
}
