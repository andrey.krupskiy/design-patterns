package and.digital.iLab.Design.patterns.factory.goksu;

/**
 * @author goksu
 * @created 17/06/2022
 * CarFactory
 **/

public abstract class CarFactory {

  public abstract Car createCar(String model);
}
