package and.digital.iLab.Design.patterns.builder.andrey.java;

public interface Transport {
  public void transport();

  public String makeSound();
}
