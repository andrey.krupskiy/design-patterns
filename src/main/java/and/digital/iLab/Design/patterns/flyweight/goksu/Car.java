package and.digital.iLab.Design.patterns.flyweight.goksu;

/**
 * @author goksu
 * @created 27/06/2022
 * Car
 **/

public interface Car {
  void printCar();
}
