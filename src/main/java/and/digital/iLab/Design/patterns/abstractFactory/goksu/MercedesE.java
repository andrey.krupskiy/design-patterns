package and.digital.iLab.Design.patterns.abstractFactory.goksu;


/**
 * @author goksu
 * @created 17/06/2022
 * MercedesE
 **/

public class MercedesE extends Car {
  private CarPartsFactory carPartsFactory;

  public MercedesE(String model, CarPartsFactory carPartsFactory) {
    super(model);
    this.carPartsFactory = carPartsFactory;
  }

  @Override
  public void assemble() {
    System.out.println("Assembling MercedesE");
    window = carPartsFactory.createWindow();
    steeringWheel = carPartsFactory.createSteeringWheel();
    wheel = carPartsFactory.createWheel();
  }
}
