package and.digital.iLab.Design.patterns.composite.goksu;

import java.util.ArrayList;
import java.util.List;

/**
 * @author goksu
 * @created 23/06/2022
 * Mercedes
 **/

public class Mercedes implements Car {
  private final List<Car> carList = new ArrayList<>();

  public void addCar(Car car) {
    carList.add(car);
  }

  @Override
  public void printModel() {
    carList.forEach(Car::printModel);
  }
}
