package and.digital.iLab.Design.patterns.facade.boyd;

public class main
{
    public static void main(String[] args) {
        RestaurantFacade restaurantFacade = new RestaurantFacade();
        restaurantFacade.serveCustomer("Spaghetti", 10);
    }
}
