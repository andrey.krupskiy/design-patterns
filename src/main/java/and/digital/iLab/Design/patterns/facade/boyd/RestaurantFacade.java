package and.digital.iLab.Design.patterns.facade.boyd;

public class RestaurantFacade {
    Restaurant restaurant = new Restaurant();
    public void serveCustomer(String food, int tableNr) {
        restaurant.TakeOrder(food, tableNr);
        restaurant.CookOrder(food);
        restaurant.TakeOutOrder(food, tableNr);
    }
}
