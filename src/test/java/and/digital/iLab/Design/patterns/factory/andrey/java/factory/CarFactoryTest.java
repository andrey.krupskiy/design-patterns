package and.digital.iLab.Design.patterns.factory.andrey.java.factory;

import static org.junit.jupiter.api.Assertions.*;

import and.digital.iLab.Design.patterns.factory.andrey.java.Transport;
import org.junit.jupiter.api.Test;

class CarFactoryTest {

  @Test
  void CreateCarTest() {
    CarFactory carFactory = new CarFactory();
    Transport car = carFactory.createTransport("car");
    assertTrue(car.MakeSound().contains("Honk-honk, I'm car and was built on"));
  }
}
