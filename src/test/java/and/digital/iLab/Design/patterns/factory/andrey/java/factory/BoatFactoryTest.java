package and.digital.iLab.Design.patterns.factory.andrey.java.factory;

import static org.junit.jupiter.api.Assertions.*;

import and.digital.iLab.Design.patterns.factory.andrey.java.Transport;
import org.junit.jupiter.api.Test;

class BoatFactoryTest {
  @Test
  void CreateGoodBoatTest() {
    try {
      BoatFactory boatFactory = new BoatFactory();
      Transport boat = boatFactory.createTransport("boat_name").WithPeopleCapacity(5);
      String sound = boat.MakeSound();
      assertTrue(sound.contains("BRRRRRRRRR I'm a boat_name boat,"));
      assertTrue(sound.contains("I have capacity of 5 people"));
    } catch (Exception e) {
    }
  }

  @Test
  void CreateTooBigBoatTest() {

    BoatFactory boatFactory = new BoatFactory();
    Exception e =
        assertThrows(
            RuntimeException.class,
            () -> {
              boatFactory.createTransport("boat_name").WithPeopleCapacity(100);
            });
  }
}
