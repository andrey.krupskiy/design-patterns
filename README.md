# design-patterns

A place to store projects built using different design patterns.

# How to use

1. Clone the repository
2. Create a branch with a pattern of your choice
3. Create a folder in the `patterns` directory. Please follow this directory structure:

```
../design-patterns/
├── README.md
└── src
    ├── main.java.and.digital.iLab.Design.patterns
        └── builder
          │   └── andrey
          │       └── ... 
          │   └── seba 
          │       └── ... 
          │   └── goksu 
          │       └── ... 
        └── command
          │   └── andrey
          │       └── ... 
          │   └── seba 
          │       └── ... 
          │   └── goksu 
          │       └── ... 
```
4. When done, push directly to master. Please make sure you don't delete/modify the code in the folders of other people!
